echo "**********************************************************"
echo "Script to Upload OTL OAF Components" 
echo "**********************************************************"
echo "Enter the apps Password"
stty -echo
read appspwd
stty echo
echo ""
echo "Enter the xxcus Password"
stty -echo
read xxcuspwd
stty echo
echo ""
echo "Enter the protocol"
read protocol
echo "Enter the host name"
read host
echo "Enter the port number"
read port
echo "Enter the service name"
read service_name
echo "**********************************************************"
echo "Executing Table" 
echo "**********************************************************"
sqlplus -s xxcus/$xxcuspwd @xx_shipments_t.sql
echo "**********************************************************"
echo "Execution of Table Completed" 
echo "**********************************************************"


echo "**********************************************************"
echo "Executing Synonym" 
echo "**********************************************************"
sqlplus -s apps/$appspwd @xx_shipments_sy.sql
echo "**********************************************************"
echo "Execution of Synonym Completed" 
echo "**********************************************************"

echo "**********************************************************"
echo "Executing Views" 
echo "**********************************************************"
sqlplus -s apps/$appspwd @xx_shipments_v.sql
echo "**********************************************************"
echo "Execution of Views Completed" 
echo "**********************************************************"

echo "**********************************************************"
echo "Executing LDT Files" 
echo "**********************************************************"
FNDLOAD apps/$appspwd O Y UPLOAD $FND_TOP/ph/115/import/aflvmlu.lct XX_SHPMT_APPRD_MOT.ldt CUSTOM_MODE=FORCE
FNDLOAD apps/$appspwd O Y UPLOAD $FND_TOP/ph/115/import/aflvmlu.lct XX_SHPMT_FST.ldt CUSTOM_MODE=FORCE
FNDLOAD apps/$appspwd O Y UPLOAD $FND_TOP/ph/115/import/aflvmlu.lct XX_SHPMT_STAT.ldt CUSTOM_MODE=FORCE
FNDLOAD apps/$appspwd O Y UPLOAD $FND_TOP/ph/115/import/aflvmlu.lct XX_SHPMT_SYS_STATUS.ldt CUSTOM_MODE=FORCE
FNDLOAD apps/$appspwd O Y UPLOAD $FND_TOP/ph/115/import/aflvmlu.lct XX_SHPMT_CONT_MOT.ldt CUSTOM_MODE=FORCE
FNDLOAD apps/$appspwd 0 Y UPLOAD $FND_TOP/ph/115/import/afsload.lct XXSHIPMTPG.ldt 
echo "**********************************************************"
echo "Execution of LDT Files" 
echo "**********************************************************"



echo "**********************************************************"
echo "Copying the Java Class files to $JAVA_TOP" 
echo "**********************************************************"
cp -R xxcus $JAVA_TOP

echo "**********************************************************"
echo "Importing Page" 
echo "**********************************************************"
java oracle.jrad.tools.xml.importer.XMLImporter $JAVA_TOP/xxcus/oracle/apps/po/shipment/webui/ShipmtPG.xml -username apps -password $appspwd -dbconnection "(DESCRIPTION=(ADDRESS=(PROTOCOL=$protocol)(HOST=$host)(PORT=$port))(CONNECT_DATA=(SID=$service_name)))" -rootdir $JAVA_TOP;
java oracle.jrad.tools.xml.importer.XMLImporter $JAVA_TOP/xxcus/oracle/apps/po/shipment/webui/AddDtlsPG.xml -username apps -password $appspwd -dbconnection "(DESCRIPTION=(ADDRESS=(PROTOCOL=$protocol)(HOST=$host)(PORT=$port))(CONNECT_DATA=(SID=$service_name)))" -rootdir $JAVA_TOP;
java oracle.jrad.tools.xml.importer.XMLImporter $JAVA_TOP/xxcus/oracle/apps/po/shipment/webui/AddShipmtPG.xml -username apps -password $appspwd -dbconnection "(DESCRIPTION=(ADDRESS=(PROTOCOL=$protocol)(HOST=$host)(PORT=$port))(CONNECT_DATA=(SID=$service_name)))" -rootdir $JAVA_TOP;
java oracle.jrad.tools.xml.importer.XMLImporter $JAVA_TOP/xxcus/oracle/apps/po/shipment/webui/UpdShipmtPG.xml -username apps -password $appspwd -dbconnection "(DESCRIPTION=(ADDRESS=(PROTOCOL=$protocol)(HOST=$host)(PORT=$port))(CONNECT_DATA=(SID=$service_name)))" -rootdir $JAVA_TOP;
java oracle.jrad.tools.xml.importer.JPXImporter $JAVA_TOP/xxcus/oracle/apps/inl/poplist/OASubmitButtonDis.jpx -username apps -password $appspwd -dbconnection "(DESCRIPTION=(ADDRESS=(PROTOCOL=$protocol)(HOST=$host)(PORT=$port))(CONNECT_DATA=(SID=$service_name)))" -rootdir $JAVA_TOP;

echo "**********************************************************"
echo "Import of OAF Page Completed" 
echo "**********************************************************"



echo "**********************************************************"
echo "Starting Reports Deployment" 
echo "**********************************************************"


echo "**********************************************************"
echo "Starting FND_LOADER" 
echo "**********************************************************"

FNDLOAD apps/$appspwd 0 Y UPLOAD $FND_TOP/ph/115/import/afcpprog.lct XX_SHIP_TRACKER_CP.ldt - WARNING=YES UPLOAD_MODE=REPLACE CUSTOM_MODE=FORCE
FNDLOAD apps/$appspwd O Y UPLOAD $XDO_TOP/ph/115/import/xdotmpl.lct XX_SHIP_TRACKER_DD.ldt
FNDLOAD apps/$appspwd 0 Y UPLOAD $FND_TOP/ph/115/import/afcpprog.lct XX_LCM_SHIP_DETAILS_CP.ldt - WARNING=YES UPLOAD_MODE=REPLACE CUSTOM_MODE=FORCE
FNDLOAD apps/$appspwd O Y UPLOAD $XDO_TOP/ph/115/import/xdotmpl.lct XX_LCM_SHIP_DETAILS_DD.ldt

echo "**********************************************************"
echo "Completing FND_LOADER" 
echo "**********************************************************"

echo "**********************************************************"
echo "Starting XDO Loader" 
echo "**********************************************************"


java oracle.apps.xdo.oa.util.XDOLoader UPLOAD \
-DB_USERNAME apps \
-DB_PASSWORD $appspwd \
-JDBC_CONNECTION $host:$port:$service_name \
-LOB_TYPE DATA_TEMPLATE \
-APPS_SHORT_NAME PO \
-LOB_CODE XX_SHIP_TRACKER \
-LANGUAGE en \
-TERRITORY 00 \
-TRANSLATE Y \
-XDO_FILE_TYPE XML \
-NLS_LANG en \
-FILE_CONTENT_TYPE 'text/html' \
-FILE_NAME XX_SHIP_TRACKER.xml \
-LOG_NAME XX_SHIP_TRACKER.xml.log \
-CUSTOM_MODE FORCE


java oracle.apps.xdo.oa.util.XDOLoader UPLOAD \
-DB_USERNAME apps \
-DB_PASSWORD $appspwd \
-JDBC_CONNECTION $host:$port:$service_name \
-LOB_TYPE DATA_TEMPLATE \
-APPS_SHORT_NAME INL \
-LOB_CODE XX_LCM_SHIP_DETAILS \
-LANGUAGE en \
-TERRITORY 00 \
-TRANSLATE Y \
-XDO_FILE_TYPE XML \
-NLS_LANG en \
-FILE_CONTENT_TYPE 'text/html' \
-FILE_NAME XX_LCM_SHIP_DETAILS.xml \
-LOG_NAME XX_LCM_SHIP_DETAILS.xml.log \
-CUSTOM_MODE FORCE


java oracle.apps.xdo.oa.util.XDOLoader UPLOAD \
-DB_USERNAME apps \
-DB_PASSWORD $appspwd \
-JDBC_CONNECTION $host:$port:$service_name \
-LOB_TYPE TEMPLATE_SOURCE \
-APPS_SHORT_NAME PO \
-LOB_CODE XX_SHIP_TRACKER \
-LANGUAGE en \
-TERRITORY 00 \
-TRANSLATE Y \
-XDO_FILE_TYPE RTF \
-NLS_LANG en \
-FILE_CONTENT_TYPE 'application/rtf' \
-FILE_NAME XX_SHIP_TRACKER.xml \
-LOG_NAME XX_SHIP_TRACKER.rtf.log \
-CUSTOM_MODE FORCE


java oracle.apps.xdo.oa.util.XDOLoader UPLOAD \
-DB_USERNAME apps \
-DB_PASSWORD $appspwd \
-JDBC_CONNECTION $host:$port:$service_name \
-LOB_TYPE TEMPLATE_SOURCE \
-APPS_SHORT_NAME INL \
-LOB_CODE XX_LCM_SHIP_DETAILS \
-LANGUAGE en \
-TERRITORY 00 \
-TRANSLATE Y \
-XDO_FILE_TYPE RTF \
-NLS_LANG en \
-FILE_CONTENT_TYPE 'application/rtf' \
-FILE_NAME XX_LCM_SHIP_DETAILS.xml \
-LOG_NAME XX_LCM_SHIP_DETAILS.rtf.log \
-CUSTOM_MODE FORCE


echo "**********************************************************"
echo "Completed XDO Loader" 
echo "**********************************************************"


echo "**********************************************************"
echo "All Components Uploaded successfully" 
echo "**********************************************************"
