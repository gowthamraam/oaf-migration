SET PAGESIZE 0 FEEDBACK OFF VERIFY OFF HEADING OFF ECHO OFF LONG 999999 LONGCHUNKSIZE 999999 LINESIZE 500

-- =========================================================================
-- $header$
--
-- name             : xx_shipments_t.sql
--
-- description      : Table creation scripts for  Allied Shipment Tracking
--
--
-- who                                            remarks                        date
-- --------------                  --------------------------------------- ------------------
-- gowtham raam.j -4iapps                     0.1 - dev version               20-OCT-2019
-- =========================================================================
/*table -1     :     Shipment header table */

CREATE TABLE XXCUS.XX_SHIPMENT_HDR
(
  SHIPMT_HDR_ID        NUMBER                   NOT NULL,
  SHIPMENT_NUMBER      NUMBER,
  SUPPLIER_NAME        VARCHAR2(2000 BYTE),
  VENDOR_ID            NUMBER,
  SUPPLIER_INVOICE     VARCHAR2(200 BYTE),
  AWB_NUMBER           VARCHAR2(200 BYTE),
  CONTRACT_MOT         VARCHAR2(240 BYTE),
  APPROVED_MOT         VARCHAR2(240 BYTE),
  FINAL_SHIPMENT_TYPE  VARCHAR2(240 BYTE),
  MOH_1ST_APPROVAL     VARCHAR2(240 BYTE),
  MOH_2ND_APPROVAL     VARCHAR2(240 BYTE),
  ETA                  DATE,
  BOE_DATE             DATE,
  BOE_NUMBER           VARCHAR2(240 BYTE),
  WAREHOUSE            DATE,
  STATUS               VARCHAR2(240 BYTE),
  SYSEM_STATUS         VARCHAR2(240 BYTE),
  SHIPMENT_SUPPLIER    VARCHAR2(240 BYTE),
  ATTRIBUTE_CATEGORY   VARCHAR2(240 BYTE),
  ATTRIBUTE1           VARCHAR2(240 BYTE),
  ATTRIBUTE2           VARCHAR2(240 BYTE),
  ATTRIBUTE3           VARCHAR2(240 BYTE),
  ATTRIBUTE4           VARCHAR2(240 BYTE),
  ATTRIBUTE5           VARCHAR2(240 BYTE),
  ATTRIBUTE6           VARCHAR2(240 BYTE),
  ATTRIBUTE7           VARCHAR2(240 BYTE),
  ATTRIBUTE8           VARCHAR2(240 BYTE),
  ATTRIBUTE9           VARCHAR2(240 BYTE),
  ATTRIBUTE10          VARCHAR2(240 BYTE),
  CREATED_BY           NUMBER                   NOT NULL,
  CREATION_DATE        DATE                     NOT NULL,
  LAST_UPDATED_BY      NUMBER                   NOT NULL,
  LAST_UPDATE_DATE     DATE                     NOT NULL,
  LAST_UPDATE_LOGIN    NUMBER
);


     
alter table xx_shipment_hdr add 
constraint xx_shipment_hdr_pk primary key (shipmt_hdr_id) using index;

---------------------------------------------------------------------------------------------------------------------------------------

/* table -2     :       Shipment detail table */

CREATE TABLE XXCUS.XX_SHIPMENT_DTL
(
  SHIPMT_DTL_ID       NUMBER                    NOT NULL,
  SHIPMT_HDR_ID       NUMBER                    NOT NULL,
  PO_HEADER_ID        NUMBER,
  SALES_ORDER_NUMBER  VARCHAR2(240 BYTE),
  PO_LINE_ID          NUMBER,
  ATTRIBUTE_CATEGORY  VARCHAR2(240 BYTE),
  ATTRIBUTE1          VARCHAR2(240 BYTE),
  ATTRIBUTE2          VARCHAR2(240 BYTE),
  ATTRIBUTE3          VARCHAR2(240 BYTE),
  ATTRIBUTE4          VARCHAR2(240 BYTE),
  ATTRIBUTE5          VARCHAR2(240 BYTE),
  ATTRIBUTE6          VARCHAR2(240 BYTE),
  ATTRIBUTE7          VARCHAR2(240 BYTE),
  ATTRIBUTE8          VARCHAR2(240 BYTE),
  ATTRIBUTE9          VARCHAR2(240 BYTE),
  ATTRIBUTE10         VARCHAR2(240 BYTE),
  CREATED_BY          NUMBER                    NOT NULL,
  CREATION_DATE       DATE                      NOT NULL,
  LAST_UPDATED_BY     NUMBER                    NOT NULL,
  LAST_UPDATE_DATE    DATE                      NOT NULL,
  LAST_UPDATE_LOGIN   NUMBER,
  ALLOCATION_QTY      NUMBER
);

alter table xx_shipment_dtl add 
constraint xx_shipment_dtl_pk primary key (shipmt_dtl_id) using index;


 

---------------------------------------------------------------------------------------------------------------------------------------

/* table -3     :    Shipment Sale Order table */

CREATE TABLE XXCUS.XX_SHIPMENT_SO
(
  SHIPMT_SO_ID        NUMBER                    NOT NULL,
  SHIPMT_DTL_ID       NUMBER                    NOT NULL,
  SHIPMT_HDR_ID       NUMBER                    NOT NULL,
  SALES_ORDER_NUMBER  VARCHAR2(4000 BYTE),
  BME_RDD             DATE,
  ALLOCATION_QTY      VARCHAR2(300 BYTE),
  ALLOCATION_DATE     DATE,
  AVAILABILITY_DATE   DATE,
  ATTRIBUTE_CATEGORY  VARCHAR2(240 BYTE),
  ATTRIBUTE1          VARCHAR2(240 BYTE),
  ATTRIBUTE2          VARCHAR2(240 BYTE),
  ATTRIBUTE3          VARCHAR2(240 BYTE),
  ATTRIBUTE4          VARCHAR2(240 BYTE),
  ATTRIBUTE5          VARCHAR2(240 BYTE),
  ATTRIBUTE6          VARCHAR2(240 BYTE),
  ATTRIBUTE7          VARCHAR2(240 BYTE),
  ATTRIBUTE8          VARCHAR2(240 BYTE),
  ATTRIBUTE9          VARCHAR2(240 BYTE),  ATTRIBUTE10         VARCHAR2(240 BYTE),
  CREATED_BY          NUMBER                    NOT NULL,
  CREATION_DATE       DATE                      NOT NULL,
  LAST_UPDATED_BY     NUMBER                    NOT NULL,
  LAST_UPDATE_DATE    DATE                      NOT NULL,
  LAST_UPDATE_LOGIN   NUMBER,
  CONTRACT_MOT        VARCHAR2(240 BYTE),
  APPROVED_MOT        VARCHAR2(240 BYTE)
);
        
     
        
        
alter table xx_shipment_so add 
constraint xx_shipment_so_pk primary key (shipmt_so_id) using index;





-------------------------------------------------------------------------------------------------------------------------------------

create sequence xx_shipmt_hdr_id_s start with 1 nocycle  nocache ;             

create sequence xx_shipmt_dtl_id_s start with 1 nocycle  nocache ;  

create sequence xx_shipmt_so_id_s start with 1 nocycle  nocache ;                  

------------------------------------------------------------------------------------------------------------------------------------- 

    --grant for tables
    
grant all on xx_shipment_hdr to apps;
grant all on xx_shipment_dtl to apps;
grant all on xx_shipment_so to apps;


     --grant for sequences

grant all on xx_shipmt_hdr_id_s to apps;
grant all on xx_shipmt_dtl_id_s to apps;
grant all on xx_shipmt_so_id_s to apps;

-------------------------------------------------------------------------------------------------------------------------------------
EXIT;
EOF