
# Table of Contents

1.  [Introduction](#orgf0cbb91)
2.  [Prerequistes](#org4f70e25)
3.  [Deployment Steps](#org296e809)
    1.  [Executing the Script](#orga7f954e)
    2.  [Upload the Personalization](#org1155ee9)
4.  [Checklist](#org0ca6c18)
    1.  [Tables](#orgec26bf6)
    2.  [Views](#orgab6b94c)
    3.  [Lookups](#org0e5a3ca)
    4.  [OAF Pages](#org9781a92)
    5.  [Reports](#orge8a56e6)
    6.  [OAF Page Personalization](#org22f0c93)



<a id="orgf0cbb91"></a>

# Introduction

-   Codepack and the script is about the deploy the Shipment Process in Oracle Applications for R12.1 version
-   Script will automatically deploy the Objects
-   Below are the Objects for the Deployment 
    -   DB Objects
    -   OAF Code Pack
    -   Reports
    -   OAF Customizations
-   Two Process will be involved for Deployment 
    -   Execution of Script
    -   Customization Import in OAF Page


<a id="org4f70e25"></a>

# Prerequistes


<a id="org296e809"></a>

# Deployment Steps

-   Create a folder in $XXCUS\_TOP named xx\_shipment
-   Copy the files to the server in the above path
-   Copy the xxcus folder and oracle folder to $JAVA\_TOP
-   Execute the Script Components\_Deployment\_Script.sh
-   Copy the oracle folder to the Profile Option of FND: Personalization Document Root Path
-   Go to Functional Administrator and import the Personalizations


<a id="orga7f954e"></a>

## Executing the Script

Execute the File named Components\_Deployment\_Script.sh by the below command

    $ sh Components_Deployment_Script.sh 

On Executing the command it will prompt for the Parameters

1.  apps Password
2.  xxcus Password
3.  Enter the protocol :: tcp
4.  Enter the host name :: DB Server Host Name
5.  Enter the port number :: DB Server Port Number
6.  Enter the service name :: DB Service Name/SID


<a id="org1155ee9"></a>

## Upload the Personalization

1.  Ensure the path has been given to Profile Option of FND: Personalization Document Root Path
2.  If the path is not mentioned give the directory path created in Deployment Steps
3.  Login to Application and navigate to Functional Administrator Responsibility
4.  Click on Personalization Tab and click on Import/Export Link
5.  Click on the Exported Personalizations Option
6.  Click on Select All and click Import to File System Button.


<a id="org0ca6c18"></a>

# Checklist


<a id="orgec26bf6"></a>

## Tables

<table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">


<colgroup>
<col  class="org-left" />

<col  class="org-left" />
</colgroup>
<thead>
<tr>
<th scope="col" class="org-left">Name</th>
<th scope="col" class="org-left">Description</th>
</tr>
</thead>

<tbody>
<tr>
<td class="org-left">XX\_SHIPMENT\_HDR</td>
<td class="org-left">Table stores Shipment Header details created by User</td>
</tr>


<tr>
<td class="org-left">XX\_SHIPMENT\_DTL</td>
<td class="org-left">Table stores Shipment Details created by selected Supplier Name and PO details</td>
</tr>


<tr>
<td class="org-left">XX\_SHIPMENT\_SO</td>
<td class="org-left">Table stores Allocation details and Sales Order detail from the selected Shipment Detail</td>
</tr>
</tbody>
</table>


<a id="orgab6b94c"></a>

## Views

<table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">


<colgroup>
<col  class="org-left" />

<col  class="org-left" />
</colgroup>
<thead>
<tr>
<th scope="col" class="org-left">Name</th>
<th scope="col" class="org-left">Description</th>
</tr>
</thead>

<tbody>
<tr>
<td class="org-left">XX\_SHIPMENT\_HDR\_V</td>
<td class="org-left">View fetches the Shipment Header record</td>
</tr>


<tr>
<td class="org-left">XX\_SHIPMENT\_DTL\_V</td>
<td class="org-left">View fetches PO Shipment Line based record with Available and Pending Quantity</td>
</tr>


<tr>
<td class="org-left">XX\_SHIPMENT\_SO\_V</td>
<td class="org-left">View fetches the Shipment Sale Order and breakdown Allocation Quantity</td>
</tr>
</tbody>
</table>


<a id="org0e5a3ca"></a>

## Lookups

<table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">


<colgroup>
<col  class="org-left" />

<col  class="org-left" />
</colgroup>
<thead>
<tr>
<th scope="col" class="org-left">Name</th>
<th scope="col" class="org-left">Description</th>
</tr>
</thead>

<tbody>
<tr>
<td class="org-left">XX\_SHPMT\_APPRD\_MOT</td>
<td class="org-left">Lookup Stores Approved MOT</td>
</tr>


<tr>
<td class="org-left">XX\_SHPMT\_FST</td>
<td class="org-left">&#xa0;</td>
</tr>


<tr>
<td class="org-left">XX\_SHPMT\_STAT</td>
<td class="org-left">Lookup Stores Status</td>
</tr>


<tr>
<td class="org-left">XX\_SHPMT\_SYS\_STATUS</td>
<td class="org-left">Lookup Stores System Status</td>
</tr>


<tr>
<td class="org-left">XX\_SHPMT\_CONT\_MOT</td>
<td class="org-left">Lookup Stores Cont MOT</td>
</tr>
</tbody>
</table>


<a id="org9781a92"></a>

## OAF Pages

<table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">


<colgroup>
<col  class="org-left" />

<col  class="org-left" />

<col  class="org-left" />
</colgroup>
<thead>
<tr>
<th scope="col" class="org-left">Name</th>
<th scope="col" class="org-left">Path</th>
<th scope="col" class="org-left">Description</th>
</tr>
</thead>

<tbody>
<tr>
<td class="org-left">ShipmtPG</td>
<td class="org-left">&#xa0;</td>
<td class="org-left">Page is used to create Shift Details by supervisor</td>
</tr>


<tr>
<td class="org-left">AddDtlsPG</td>
<td class="org-left">&#xa0;</td>
<td class="org-left">Page is used to update any shift details which are not assigned to an employee by supervisor.</td>
</tr>


<tr>
<td class="org-left">UpdShipmtPG</td>
<td class="org-left">&#xa0;</td>
<td class="org-left">Update the Shipment Page.</td>
</tr>


<tr>
<td class="org-left">AddShipmtPG</td>
<td class="org-left">&#xa0;</td>
<td class="org-left">Create a new Shipment Header</td>
</tr>
</tbody>
</table>


<a id="orge8a56e6"></a>

## Reports

<table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">


<colgroup>
<col  class="org-left" />

<col  class="org-left" />
</colgroup>
<thead>
<tr>
<th scope="col" class="org-left">Name</th>
<th scope="col" class="org-left">Description</th>
</tr>
</thead>

<tbody>
<tr>
<td class="org-left">XX\_SHIP\_TRACKER</td>
<td class="org-left">BME Shipment Tracker Report</td>
</tr>


<tr>
<td class="org-left">XX\_LCM\_SHIP\_DETAILS</td>
<td class="org-left">BME LCM Shipment Details Report</td>
</tr>
</tbody>
</table>


<a id="org22f0c93"></a>

## OAF Page Personalization

<table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">


<colgroup>
<col  class="org-left" />

<col  class="org-left" />
</colgroup>
<thead>
<tr>
<th scope="col" class="org-left">Name</th>
<th scope="col" class="org-left">Path</th>
</tr>
</thead>

<tbody>
<tr>
<td class="org-left">ShipmentCreatePG</td>
<td class="org-left">&#xa0;</td>
</tr>


<tr>
<td class="org-left">ShipmentUpdatePG</td>
<td class="org-left">&#xa0;</td>
</tr>
</tbody>
</table>

