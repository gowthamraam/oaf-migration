SET PAGESIZE 0 FEEDBACK OFF VERIFY OFF HEADING OFF ECHO OFF LONG 999999 LONGCHUNKSIZE 999999 LINESIZE 500
-- =========================================================================
-- $header$
--
-- name      : xxadt_grievances_v.sql
--
-- description     : view creation scripts for  xxadt_grievance custom module
--
--
-- who                                            remarks                        date
-- --------------                  --------------------------------------- ------------------
-- gowtham raam.j -4iapps                     0.1 - dev version               02-jun-2014
-- =========================================================================
/*      view - 1      :     grievance header view  */

CREATE OR REPLACE FORCE VIEW APPS.XX_SHIPMENT_HDR_V
(
    ROW_ID,
    SHIPMT_HDR_ID,
    SHIPMENT_NUMBER,
    SUPPLIER_NAME,
    VENDOR_ID,
    SUPPLIER_INVOICE,
    AWB_NUMBER,
    CONTRACT_MOT,
    APPROVED_MOT,
    FINAL_SHIPMENT_TYPE,
    FINAL_SHIPMENT_TYPE_DESC,
    MOH_1ST_APPROVAL,
    MOH_2ND_APPROVAL,
    ETA,
    BOE_DATE,
    BOE_NUMBER,
    WAREHOUSE,
    STATUS,
    STATUS_DESC,
    SYSEM_STATUS,
    SHIPMENT_SUPPLIER,
    ATTRIBUTE_CATEGORY,
    ATTRIBUTE1,
    ATTRIBUTE2,
    ATTRIBUTE3,
    ATTRIBUTE4,
    ATTRIBUTE5,
    ATTRIBUTE6,
    ATTRIBUTE7,
    ATTRIBUTE8,
    ATTRIBUTE9,
    ATTRIBUTE10,
    CREATED_BY,
    CREATION_DATE,
    LAST_UPDATED_BY,
    LAST_UPDATE_DATE,
    LAST_UPDATE_LOGIN
)
AS
    SELECT ROWID
               row_id,
           shipmt_hdr_id,
           shipment_number,
           --creation_date  date,
           supplier_name,
           vendor_id,
           supplier_invoice,
           awb_number,
           contract_mot,
           approved_mot,
           final_shipment_type,
           (SELECT MEANING
              FROM fnd_lookup_values
             WHERE     lookup_type = 'XX_SHPMT_APPRD_MOT'
                   AND LOOKUP_CODE = xgh.final_shipment_type)
               final_shipment_type_desc,
           moh_1st_approval,
           moh_2nd_approval,
           eta,
           boe_date,
           boe_number,
           warehouse,
           status,
           (SELECT MEANING
              FROM fnd_lookup_values
             WHERE     lookup_type = 'XX_SHPMT_STAT'
                   AND LOOKUP_CODE = xgh.status)
               status_desc,
           sysem_status,
           shipment_supplier,
           --supplier_name  ,
           attribute_category,
           attribute1,
           attribute2,
           attribute3,
           attribute4,
           attribute5,
           attribute6,
           attribute7,
           attribute8,
           attribute9,
           attribute10,
           created_by,
           creation_date,
           last_updated_by,
           last_update_date,
           last_update_login
      FROM xx_shipment_hdr xgh;
 

---------------------------------------------------------------------------------------------------------------------------------------

/*      view -2      :     Shipment detail view  */


 
CREATE OR REPLACE FORCE VIEW APPS.XX_SHIPMENT_DTL_V
(
    ROW_ID,
    SHIPMT_DTL_ID,
    SHIPMT_HDR_ID,
    PO_HEADER_ID,
    PO_LINE_ID,
    SALES_ORDER_NUMBER,
    PO_NUMBER,
    PO_DATE,
    CURRENCY,
    PO_STATUS,
    BUYER_NAME,
    PO_LINE_NUMBER,
    ITEM_CODE,
    ITEM_DESCRIPTION,
    PRIMARY_UNIT_OF_MEASURE,
    UNIT_PRICE,
    ORDERED_QTY,
    QUANTITY_RECEIVED,
    QUANTITY_BILLED,
    PENDING_QTY,
    PENDING_VALUE,
    AVAILABLE_QTY,
    ALLOCATION_QTY,
    AVL_QTY,
    FAMILY,
    SUB_FAMILY,
    ATTRIBUTE_CATEGORY,
    ATTRIBUTE1,
    ATTRIBUTE2,
    ATTRIBUTE3,
    ATTRIBUTE4,
    ATTRIBUTE5,
    ATTRIBUTE6,
    ATTRIBUTE7,
    ATTRIBUTE8,
    ATTRIBUTE9,
    ATTRIBUTE10,
    CREATED_BY,
    CREATION_DATE,
    LAST_UPDATED_BY,
    LAST_UPDATE_DATE,
    LAST_UPDATE_LOGIN,
    VENDOR_ID
)
AS
    SELECT xad.ROWID
               row_id,
           xad.shipmt_dtl_id,
           xad.shipmt_hdr_id,
           pha.po_header_id,
           pla.po_line_id,
           xad.sales_order_number,
           pha.segment1
               po_number,
           pha.creation_date
               po_date,
           pha.currency_code
               currency,
           NVL (pla.CLOSED_CODE, 'Open')
               po_status,
           pbav.FULL_NAME
               buyer_name,
           pla.line_num
               po_line_number,
           msib.segment1
               item_code,
           msib.description
               item_description,
           msib.primary_unit_of_measure,
           pla.unit_price,
           pla.quantity
               ordered_qty,
           (SELECT SUM (NVL (quantity_received, 0))
              FROM po_line_locations_all plla
             WHERE plla.po_line_id = pla.po_line_id)
               quantity_received,
           (SELECT SUM (NVL (quantity_billed, 0))
              FROM po_line_locations_all plla
             WHERE plla.po_line_id = pla.po_line_id)
               quantity_billed,
             pla.quantity
           - (SELECT SUM (NVL (quantity_received, 0))
                FROM po_line_locations_all plla
               WHERE plla.po_line_id = pla.po_line_id)
               pending_qty,
             (  pla.quantity
              - (SELECT SUM (NVL (quantity_received, 0))
                   FROM po_line_locations_all plla
                  WHERE plla.po_line_id = pla.po_line_id))
           * pla.unit_price
               pending_value,
             pla.quantity
           - ((SELECT NVL (
                          SUM (
                              CASE
                                  WHEN xsdh.sysem_status = 'Cancelled' THEN 0
                                  ELSE ALLOCATION_QTY
                              END),
                          0)
                 FROM xx_shipment_dtl xsdv, xx_shipment_hdr xsdh
                WHERE     xsdv.po_header_id = pha.PO_HEADER_ID
                      AND xsdv.po_line_id = pla.PO_LINE_ID
                      AND xsdv.shipmt_hdr_id = xsdh.shipmt_hdr_id))
               available_qty,
           xad.ALLOCATION_QTY,
           (SELECT SUM (
                       NVL (
                           CASE
                               WHEN xsdh.sysem_status = 'Cancelled' THEN 0
                               ELSE ALLOCATION_QTY
                           END,
                           0))
              FROM xx_shipment_dtl xsdv, xx_shipment_hdr xsdh
             WHERE     xsdv.po_header_id = pha.PO_HEADER_ID
                   AND xsdv.po_line_id = pla.PO_LINE_ID
                   AND xsdv.shipmt_hdr_id = xsdh.shipmt_hdr_id)
               avl_qty,
           mc.segment3
               family_,
           mc.segment4
               sub_family,
           xad.attribute_category,
           xad.attribute1,
           xad.attribute2,
           xad.attribute3,
           xad.attribute4,
           xad.attribute5,
           xad.attribute6,
           xad.attribute7,
           xad.attribute8,
           xad.attribute9,
           xad.attribute10,
           xad.created_by,
           xad.creation_date,
           xad.last_updated_by,
           xad.last_update_date,
           xad.last_update_login,
           pha.vendor_id
      FROM xx_shipment_dtl    xad,
           po_headers_all        pha,
           ap_suppliers          asup,
           po_buyers_all_v       pbav,
           po_lines_all          pla,
           mtl_system_items_b    msib,
           mtl_item_categories   mic,
           mtl_categories_b_kfv  mc
     WHERE     xad.po_header_id(+) = pla.po_header_id
           AND xad.po_line_id(+) = pla.po_line_id
           AND pha.po_header_id = pla.po_header_id
           AND pbav.employee_id = pha.agent_id
           AND pha.vendor_id = asup.vendor_id
           AND pla.item_id = msib.inventory_item_id
           AND msib.organization_id = pla.org_id
           AND mic.CATEGORY_SET_ID IN
                   (SELECT DISTINCT CATEGORY_SET_ID
                      FROM mtl_category_sets
                     WHERE CATEGORY_SET_NAME = 'BME Item Category')
           AND msib.organization_id = mic.organization_id(+)
           AND msib.inventory_item_id = mic.inventory_item_id(+)
           AND mic.category_id = mc.category_id;



---------------------------------------------------------------------------------------------------------------------------------------

/*      view -3     :     grievance response view  */


CREATE OR REPLACE FORCE VIEW APPS.XX_SHIPMENT_SO_V
(
    ROW_ID,
    SHIPMT_SO_ID,
    SHIPMT_DTL_ID,
    SHIPMT_HDR_ID,
    SALES_ORDER_NUMBER,
    BME_RDD,
    ALLOCATION_QTY,
    ALLOCATION_DATE,
    AVAILABILITY_DATE,
    CONTRACT_MOT,
    APPROVED_MOT,
    ATTRIBUTE_CATEGORY,
    ATTRIBUTE1,
    ATTRIBUTE2,
    ATTRIBUTE3,
    ATTRIBUTE4,
    ATTRIBUTE5,
    ATTRIBUTE6,
    ATTRIBUTE7,
    ATTRIBUTE8,
    ATTRIBUTE9,
    ATTRIBUTE10,
    CREATED_BY,
    CREATION_DATE,
    LAST_UPDATED_BY,
    LAST_UPDATE_DATE,
    LAST_UPDATE_LOGIN
)
AS
    SELECT ROWID     row_id,
           shipmt_so_id,
           shipmt_dtl_id,
           shipmt_hdr_id,
           Sales_Order_Number,
           BME_RDD,
           Allocation_Qty,
           Allocation_Date,
           Availability_Date,
           CONTRACT_MOT,
           APPROVED_MOT,
           attribute_category,
           attribute1,
           attribute2,
           attribute3,
           attribute4,
           attribute5,
           attribute6,
           attribute7,
           attribute8,
           attribute9,
           attribute10,
           created_by,
           creation_date,
           last_updated_by,
           last_update_date,
           last_update_login
      FROM xx_shipment_so xss;


---------------------------------------------------------------------------------------------------------------------------------------
EXIT;
EOF