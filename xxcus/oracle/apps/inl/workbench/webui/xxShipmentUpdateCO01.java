package xxcus.oracle.apps.inl.workbench.webui;

import oracle.apps.fnd.framework.webui.OAPageContext;
import oracle.apps.fnd.framework.webui.beans.OAWebBean;
import oracle.apps.inl.workbench.webui.ShipmentUpdateCO;

public class xxShipmentUpdateCO01 extends ShipmentUpdateCO {
    
    public void processRequest(OAPageContext pageContext, OAWebBean webBean)
    {
        super.processRequest(pageContext, webBean);
    }
    public void processFormRequest(OAPageContext pageContext, OAWebBean webBean)
    {
        
        pageContext.writeDiagnostics(this, "Inside xxShipmentUpdateCO01 ", 4);
        if(pageContext.getParameter("GoTaskButton") != null)
        {
            pageContext.writeDiagnostics(this, "Inside xxShipmentUpdateCO01 GoTaskButton has been Clicked", 4);
            String s5 = pageContext.getParameter("ShipmtTasks");
            pageContext.writeDiagnostics(this, "Inside xxShipmentUpdateCO01 GoTaskButton has been Clicked and Value of ShipmtTasks"+s5, 4);
        }
        super.processFormRequest(pageContext, webBean);
    }
}
