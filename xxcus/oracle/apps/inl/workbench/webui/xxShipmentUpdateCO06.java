package xxcus.oracle.apps.inl.workbench.webui;

import java.sql.CallableStatement;

import java.sql.SQLException;

import oracle.apps.fnd.framework.OAApplicationModule;
import oracle.apps.fnd.framework.OAException;
import oracle.apps.fnd.framework.OAViewObject;
import oracle.apps.fnd.framework.server.OADBTransaction;
import oracle.apps.fnd.framework.webui.OAPageContext;
import oracle.apps.fnd.framework.webui.OAWebBeanConstants;
import oracle.apps.fnd.framework.webui.beans.OADescriptiveFlexBean;
import oracle.apps.fnd.framework.webui.beans.OAWebBean;
import oracle.apps.fnd.framework.webui.beans.message.OAMessageLovInputBean;
import oracle.apps.inl.workbench.webui.ShipmentUpdateCO;

public class xxShipmentUpdateCO07 extends ShipmentUpdateCO {
    
    public void processRequest(OAPageContext pageContext, OAWebBean webBean)
    {
        super.processRequest(pageContext, webBean);
    }
    public void processFormRequest(OAPageContext pageContext, OAWebBean webBean)
    {
        OAApplicationModule oam = pageContext.getApplicationModule(webBean);
        if(pageContext.getParameter("GoTaskButton") != null)
        {
            pageContext.writeDiagnostics(this, "Inside xxShipmentUpdateCO04 GoTaskButton has been Clicked", 4);
            String s5 = pageContext.getParameter("ShipmtTasks");
            pageContext.writeDiagnostics(this, "Inside xxShipmentUpdateCO01 GoTaskButton has been Clicked and Value of ShipmtTasks"+s5, 4);
            
//            OADescriptiveFlexBean dff=(OADescriptiveFlexBean)webBean.findChildRecursive("DescriptFlex");
//            pageContext.writeDiagnostics(this,"PFR Value---------------"+dff,4);
//            OAMessageLovInputBean mti6=(OAMessageLovInputBean)dff.findChildRecursive("DescriptFlex1"); 
//            pageContext.writeDiagnostics(this,"PFR Value---------------"+dff,4);
//                       mti6.getValue(pageContext);  
//                       pageContext.writeDiagnostics(this,"PFR Value--"+mti6.getValue(pageContext),4);
//            DESCRIPTIVE_FLEX_BEAN kff=(DescriptiveFlexField)dff.getAttributeValue(OAWebBeanConstants.FLEXFIELD_REFERENCE);       //Getting the Reference Value
//            if(kff!=null){
//            if(kff.getSegment(1).getValue()!=null){
//            Segment1 = kff.getSegment(1).getValue().toString();    //This returns Value object so we can just see a object
//            Segment5 = kff.getSegment(1).getName();                 // This returns the Segment name attached
//
//            Segment4 = kff.getSegment(1).getInputValue();       // This returns the Segment value when user gives as input
//            pageContext.writeDiagnostics(this,�On Getting Value the Entered Value �+Segment4,1);
//            pageContext.writeDiagnostics(this,�On Getting Value the Computed  Value �+outParamValue,1);
//
//            int enter_val = Integer.parseInt(Segment4);
//            pageContext.writeDiagnostics(this,�On Getting Value the Entered Value �+enter_val,1);
//            if(outParamValue < enter_val)
//            {
//            throw new OAException(�Please Enter a lower value than your Credit Limit�+outParamValue, OAException.ERROR);
//            }
//            }
//            }


            
            //WOrking Code 
            String s3 = pageContext.getParameter("shipNum");
            OAViewObject vo = (OAViewObject)oam.findViewObject("ShipmentHeadersVO1");
            pageContext.writeDiagnostics(this, "Inside xxShipmentUpdateCO01 GoTaskButton has been Clicked and Value of Attribute1  from VO"+(vo.getCurrentRow().getAttribute("Attribute1").toString()), 4);
            String s4 =  vo.getCurrentRow().getAttribute("Attribute1").toString() ;
            
            String s2 = pageContext.getParameter("shipHdrId");
            pageContext.writeDiagnostics(this, "Inside xxShipmentUpdateCO01 GoTaskButton has been Clicked and Value of shipNum: "+s3+" shipHdrId: "+s2, 4);
            if(pageContext.getParameter("ShipmtTasks").equals("SUBMIT"))
            {
                OADBTransaction txn = oam.getOADBTransaction();
                    
                          //CallableStatement cs1 = txn.createCallableStatement("begin UPDATE xx_shipment_hdr SET SYSEM_STATUS = 'Closed' WHERE SHIPMENT_NUMBER = "+Integer.parseInt(s3)+"; COMMIT; end;", 1);
                          CallableStatement cs1 = txn.createCallableStatement("begin UPDATE xx_shipment_hdr SET SYSEM_STATUS = 'Closed' WHERE SHIPMENT_NUMBER = "+Integer.parseInt(s4)+"; COMMIT; end;", 1);
                          pageContext.writeDiagnostics(this, " Inside xxShipmentUpdateCO04 Callable Statement got Created"+cs1,4);
                          try
                          {
                              pageContext.writeDiagnostics(this, " Inside xxShipmentUpdateCO04 Before Execute" ,4);
                            cs1.execute();
                            cs1.close();pageContext.writeDiagnostics(this, " Inside xxShipmentUpdateCO04 Execute Complete " ,4);
                          }
                          ch (SQLException sqle)
                          {
                              pageContext.writeDiagnostics(this, " Inside xxShipmentUpdateCO04  Exception"+sqle ,4);
                            throw OAException.wrapperException(sqle);
                          }
            }
        }
        pageContext.writeDiagnostics(this, "Inside xxShipmentUpdateCO04 ", 4);
    }
}
