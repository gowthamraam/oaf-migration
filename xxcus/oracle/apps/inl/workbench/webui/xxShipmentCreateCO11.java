package xxcus.oracle.apps.inl.workbench.webui;

import java.sql.CallableStatement;
import java.sql.SQLException;

import oracle.apps.fnd.framework.OAApplicationModule;
import oracle.apps.fnd.framework.OAException;
import oracle.apps.fnd.framework.OAViewObject;
import oracle.apps.fnd.framework.server.OADBTransaction;
import oracle.apps.fnd.framework.webui.OAPageContext;
import oracle.apps.fnd.framework.webui.OAWebBeanConstants;
import oracle.apps.fnd.framework.webui.beans.OADescriptiveFlexBean;
import oracle.apps.fnd.framework.webui.beans.OAWebBean;
import oracle.apps.fnd.framework.webui.beans.message.OAMessageLovInputBean;
import oracle.apps.fnd.framework.webui.beans.message.OAMessageTextInputBean;
import oracle.apps.inl.workbench.webui.ShipmentCreateCO;

public class xxShipmentCreateCO12 extends ShipmentCreateCO {
    public void processRequest(OAPageContext pageContext, OAWebBean webBean)
    {
        super.processRequest(pageContext, webBean);
    }
    public void processFormRequest(OAPageContext pageContext, OAWebBean webBean)
    {
        super.processFormRequest(pageContext, webBean);
        pageContext.writeDiagnostics(this, "Inside xxShipmentUpdateCO04 ", 4);
 
        OAApplicationModule oam = pageContext.getApplicationModule(webBean);
        if(pageContext.getParameter("GoTaskButton") != null)
        {
            pageContext.writeDiagnostics(this, "Inside xxShipmentUpdateCO04 GoTaskButton has been Clicked", 4);
            String s5 = pageContext.getParameter("ShipmtTasks");
            pageContext.writeDiagnostics(this, "Inside xxShipmentUpdateCO01 GoTaskButton has been Clicked and Value of ShipmtTasks"+s5, 4);
            String s3 = pageContext.getParameter("ShipmentNum");
            pageContext.writeDiagnostics(this, "Inside xxShipmentUpdateCO01 GoTaskButton has been Clicked and Value of ShipmentNum"+s3, 4);
            OAViewObject vo = (OAViewObject)oam.findViewObject("ShipmentHeadersVO1");
            String s4 =  vo.getCurrentRow().getAttribute("ShipNum").toString() ;
            pageContext.writeDiagnostics(this, "Inside xxShipmentUpdateCO01 GoTaskButton has been Clicked and Value of ShipNum from VO"+s4, 4);
            String s2 = pageContext.getParameter("shipHdrId");
            pageContext.writeDiagnostics(this, "Inside xxShipmentUpdateCO01 GoTaskButton has been Clicked and Value of shipNum: "+s3+" shipHdrId: "+s2, 4);
            
            OADescriptiveFlexBean dff=(OADescriptiveFlexBean)webBean.findChildRecursive("DescriptFlex");
            pageContext.writeDiagnostics(this,"PFR Value---------------"+dff,4);
            
          
             Object dffval =  (Object)dff.getAttributeValue("DescriptFlex1");
            pageContext.writeDiagnostics(this,"PFR Value---------dffval------"+dffval,4);
            OAMessageLovInputBean mti6=(OAMessageLovInputBean)dff.findChildRecursive("DescriptFlex1"); 
            pageContext.writeDiagnostics(this,"PFR Value---------mti6------"+mti6,4);
            OAMessageTextInputBean  mti1=(OAMessageTextInputBean)dff.findChildRecursive("DescriptFlex1"); 
            pageContext.writeDiagnostics(this,"PFR Value---------mti1------"+mti1,4);
                      // mti6.getValue(pageContext);
//                       pageContext.writeDiagnostics(this,"PFR Value---------mti6.getValue(pageContext)------"+mti6.getValue(pageContext),4);
            
            
            
            if(pageContext.getParameter("ShipmtTasks").equals("SUBMIT"))
            {
                OADBTransaction txn = oam.getOADBTransaction();
                    
                          CallableStatement cs1 = txn.createCallableStatement("begin UPDATE xx_shipment_hdr SET SYSEM_STATUS = 'Closed' WHERE SHIPMENT_NUMBER = "+Integer.parseInt(s4)+"; COMMIT; end;", 1);
                          pageContext.writeDiagnostics(this, " Inside xxShipmentUpdateCO04 Callable Statement got Created"+cs1,4);
                          try
                          {
                              pageContext.writeDiagnostics(this, " Inside xxShipmentUpdateCO04 Before Execute" ,4);
                            cs1.execute();
                            cs1.close();pageContext.writeDiagnostics(this, " Inside xxShipmentUpdateCO04 Execute Complete " ,4);
                          }
                          ch (SQLException sqle)
                          {
                              pageContext.writeDiagnostics(this, " Inside xxShipmentUpdateCO04  Exception"+sqle ,4);
                            throw OAException.wrapperException(sqle);
                          }
            }
        }
        
    }
}
