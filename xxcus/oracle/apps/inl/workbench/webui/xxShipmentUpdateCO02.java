package xxcus.oracle.apps.inl.workbench.webui;

import oracle.apps.fnd.framework.webui.OAPageContext;
import oracle.apps.fnd.framework.webui.beans.OAWebBean;
import oracle.apps.inl.workbench.webui.ShipmentUpdateCO;

public class xxShipmentUpdateCO02 extends ShipmentUpdateCO {
    
    public void processRequest(OAPageContext pageContext, OAWebBean webBean)
    {
        super.processRequest(pageContext, webBean);
    }
    public void processFormRequest(OAPageContext pageContext, OAWebBean webBean)
    {
        
        pageContext.writeDiagnostics(this, "Inside xxShipmentUpdateCO01 ", 4);
        if(pageContext.getParameter("GoTaskButton") != null)
        {
            pageContext.writeDiagnostics(this, "Inside xxShipmentUpdateCO01 GoTaskButton has been Clicked", 4);
            String s5 = pageContext.getParameter("ShipmtTasks");
            pageContext.writeDiagnostics(this, "Inside xxShipmentUpdateCO01 GoTaskButton has been Clicked and Value of ShipmtTasks"+s5, 4);
            String s3 = pageContext.getParameter("shipNum");
            String s2 = pageContext.getParameter("shipHdrId");
            pageContext.writeDiagnostics(this, "Inside xxShipmentUpdateCO01 GoTaskButton has been Clicked and Value of shipNum: "+s3+" shipHdrId: "+s2, 4);
        }
        super.processFormRequest(pageContext, webBean);
    }
}
