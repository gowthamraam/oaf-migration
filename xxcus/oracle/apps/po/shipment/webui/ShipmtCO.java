/*===========================================================================+
 |   Copyright (c) 2001, 2005 Oracle Corporation, Redwood Shores, CA, USA    |
 |                         All rights reserved.                              |
 +===========================================================================+
 |  HISTORY                                                                  |
 +===========================================================================*/
package xxcus.oracle.apps.po.shipment.webui;

//import com.sun.java.util.collections.ArrayList;
import com.sun.java.util.collections.HashMap;
//import com.sun.java.util.collections.List;
import java.util.List;
import java.util.ArrayList;

import java.io.Serializable;

import java.util.Enumeration;

import java.util.HashSet;
import java.util.Set;

import oracle.apps.fnd.common.MessageToken;
import oracle.apps.fnd.common.VersionInfo;
import oracle.apps.fnd.framework.OAApplicationModule;
import oracle.apps.fnd.framework.OAException;
import oracle.apps.fnd.framework.OARow;
import oracle.apps.fnd.framework.OAViewObject;
import oracle.apps.fnd.framework.server.OADBTransaction;
import oracle.apps.fnd.framework.webui.OAControllerImpl;
import oracle.apps.fnd.framework.webui.OADialogPage;
import oracle.apps.fnd.framework.webui.OAPageContext;
import oracle.apps.fnd.framework.webui.OAWebBeanConstants;
import oracle.apps.fnd.framework.webui.beans.OAWebBean;

import oracle.jbo.Row;
import oracle.jbo.RowSetIterator;

import xxcus.oracle.apps.po.shipment.server.shipDtlVORowImpl;
import xxcus.oracle.apps.po.shipment.server.shipSoEORowImpl;
import xxcus.oracle.apps.CommonClass;
import xxcus.oracle.apps.po.shipment.server.shipmtHdrVORowImpl;

/**
 * Controller for ...
 */
public class ShipmtCO extends OAControllerImpl
{
  public static final String RCS_ID="$Header$";
  public static final boolean RCS_ID_RECORDED =
        VersionInfo.recordClassVersion(RCS_ID, "%packagename%");

  /**
   * Layout and page setup logic for a region.
   * @param pageContext the current OA page context
   * @param webBean the web bean corresponding to the region
   */
  public void processRequest(OAPageContext pageContext, OAWebBean webBean)
  {
    super.processRequest(pageContext, webBean);
      OAApplicationModule am=null;
      am=(OAApplicationModule)pageContext.getApplicationModule(webBean);
      OAViewObject hvo= null;
      hvo=(OAViewObject)am.findViewObject("shipmtHdrVO1");
      OAViewObject dtlVO = null;
      dtlVO = (OAViewObject)am.findViewObject("shipDtlVO1");
      OAViewObject soVO = null;
      OAViewObject staVO = null;
      soVO = (OAViewObject)am.findViewObject("shipSoEO1");
      staVO = (OAViewObject)am.findViewObject("StatusHideVO1"); 
      Serializable[] sn1={"StatusHideVO1"};
      am.invokeMethod("createRecordStHide", sn1); 
      OAViewObject expVO = null;
      expVO = (OAViewObject)am.findViewObject("exportShipmtVO1");
      pageContext.writeDiagnostics(this, "Getting  Param -----------------------> SHIPMTHDRID"+pageContext.getParameter("SHIPMTHDRID")+" PGMODE "+pageContext.getParameter("PGMODE"), 4);
//      System.out.println("Getting  Param -----------------------> SHIPMTHDRID"+pageContext.getParameter("SHIPMTHDRID")+" PGMODE "+pageContext.getParameter("PGMODE"));
        if(pageContext.getParameter("SHIPMTHDRID")!=null&&pageContext.getParameter("PGMODE")!=null)
        {
            if(pageContext.getParameter("PGMODE").equals("ADDDTLS"))
            {
                hvo.setWhereClause("SHIPMT_HDR_ID = "+pageContext.getParameter("SHIPMTHDRID"));                            
                hvo.executeQuery();
                hvo.first();
                dtlVO.setWhereClause("SHIPMT_HDR_ID = "+pageContext.getParameter("SHIPMTHDRID"));    
                dtlVO.executeQuery();
                OAException confirmMessage = new OAException("The Lines for Shipment Header "+hvo.getCurrentRow().getAttribute("ShipmentNumber")+" has been saved Successfully", OAException.CONFIRMATION);     
                System.out.println("Inside SAVE Logic Before throwing Exception ----------------------->");
                pageContext.putDialogMessage(confirmMessage);
            }      
            if(pageContext.getParameter("PGMODE").equals("UPDSHIPMT"))
            {
                hvo.setWhereClause("SHIPMT_HDR_ID = "+pageContext.getParameter("SHIPMTHDRID"));                            
                hvo.executeQuery();
                hvo.first();
//                dtlVO.setWhereClause("SHIPMT_HDR_ID = "+pageContext.getParameter("SHIPMTHDRID"));    
//                hvo.executeQuery();
                OAException confirmMessage = new OAException("The Shipment Header "+hvo.getCurrentRow().getAttribute("ShipmentNumber")+" has been Updated Successfully", OAException.CONFIRMATION);     
                System.out.println("Inside SAVE Logic Before throwing Exception ----------------------->");
                pageContext.putDialogMessage(confirmMessage);
            }    
            if(pageContext.getParameter("PGMODE").equals("ADDSHIPMT"))
            {
                hvo.setWhereClause("SHIPMT_HDR_ID = "+pageContext.getParameter("SHIPMTHDRID"));                            
                hvo.executeQuery();
                hvo.first();
                //                dtlVO.setWhereClause("SHIPMT_HDR_ID = "+pageContext.getParameter("SHIPMTHDRID"));
                //                hvo.executeQuery();
               // OAException confirmMessage = new OAException("The Shipment Header "+pageContext.getParameter("SHIPMTHDRID")+" has been Saved Successfully", OAException.CONFIRMATION);     
                 OAException confirmMessage = new OAException("The Shipment Header "+hvo.getCurrentRow().getAttribute("ShipmentNumber")+" has been Saved Successfully", OAException.CONFIRMATION);     
                System.out.println("Inside SAVE Logic Before throwing Exception ----------------------->");
                pageContext.putDialogMessage(confirmMessage);
            }  
        }
  }

  /**
   * Procedure to handle form submissions for form elements in
   * a region.
   * @param pageContext the current OA page context
   * @param webBean the web bean corresponding to the region
   */
  public void processFormRequest(OAPageContext pageContext, OAWebBean webBean)
  {
    super.processFormRequest(pageContext, webBean);
      int counter = 0;
      int counterS = 0;
    pageContext.writeDiagnostics(this, "Inside ShipmtCO We have the Source parameter ----------------------->"+SOURCE_PARAM,4);
      pageContext.writeDiagnostics(this, "Inside ShipmtCO We have the EVENT parameter ----------------------->"+EVENT_PARAM,4);
      System.out.println("We have the Source parameter ----------------------->"+SOURCE_PARAM);
         System.out.println("We have the EVENT parameter ----------------------->"+EVENT_PARAM);
      System.out.println("Getting Event Param ----------------------->"+pageContext.getParameter(EVENT_PARAM));
      OAApplicationModule am=null;
      am=(OAApplicationModule)pageContext.getApplicationModule(webBean);
      OAViewObject hvo= null;
      hvo=(OAViewObject)am.findViewObject("shipmtHdrVO1");
      OAViewObject dtlVO = null;
      dtlVO = (OAViewObject)am.findViewObject("shipDtlVO1");
      OAViewObject soVO = null;
      soVO = (OAViewObject)am.findViewObject("shipSoEO1");
      OAViewObject expVO = null;
      expVO = (OAViewObject)am.findViewObject("exportShipmtVO1");
      /**=======================================================================
         *            CREATE ADDSHIPMENT 
         * ====================================================================**/ 
        if(pageContext.getParameter(EVENT_PARAM).equals("ADDSHIPMENT"))
          {
              pageContext.writeDiagnostics(this, "Inside ADDSHIPMENT Logic ----------------------->",4);
              System.out.println("Inside ADDSHIPMENT Logic ----------------------->");
              
              pageContext.setForwardURL("OA.jsp?page=/xxcus/oracle/apps/po/shipment/webui/AddShipmtPG",
              null,
              //OAWebBeanConstants.KEEP_NO_DISPLAY_MENU_CONTEXT,
               OAWebBeanConstants.KEEP_MENU_CONTEXT,
              null,
              null, //HashMap
              
              true,
              OAWebBeanConstants.ADD_BREAD_CRUMB_YES,
              OAWebBeanConstants.IGNORE_MESSAGES); 

//              Serializable[] sn1={"shipmtHdrVO1"};
//              am.invokeMethod("createRecord", sn1); 
          }
    /**=======================================================================
    *            UPDATE ADDSHIPMENT 
    * ====================================================================**/ 
          
         if(pageContext.getParameter(EVENT_PARAM).equals("UPDATEHDR"))
           {
               pageContext.writeDiagnostics(this, "Inside UPDATEHDR Logic ----------------------->"+pageContext.getParameter(OAWebBeanConstants.EVENT_SOURCE_ROW_REFERENCE),4);
               System.out.println("Inside UPDATEHDR Logic ----------------------->");
               String rowReference = pageContext.getParameter(OAWebBeanConstants.EVENT_SOURCE_ROW_REFERENCE);
               if(rowReference!=null)
               {
                   pageContext.writeDiagnostics(this, "Inside UPDATEHDR Logic and Obtaining Row Reference ----------------------->",4);
                   OARow row = (OARow)am.findRowByRef(rowReference);
                   if(row!=null)
                   {
                       pageContext.writeDiagnostics(this, "Inside UPDATEHDR Logic and Obtaining Row Reference with Getting Update Header Row ----------------------->"+row.getAttribute("SysemStatus"),4);
//                       if((row.getAttribute("SysemStatus").toString()).equals("Cancelled"))
//                       {
//                       
//                       }
//                       else
//                       {
//                            
//                       }
                   }
               }
               
    
               
               pageContext.setForwardURL("OA.jsp?page=/xxcus/oracle/apps/po/shipment/webui/UpdShipmtPG",
               null,
               //OAWebBeanConstants.KEEP_NO_DISPLAY_MENU_CONTEXT,
                OAWebBeanConstants.KEEP_MENU_CONTEXT,
               null,
               null, //HashMap
               
               true,
               OAWebBeanConstants.ADD_BREAD_CRUMB_YES,
               OAWebBeanConstants.IGNORE_MESSAGES); 
        
         //              Serializable[] sn1={"shipmtHdrVO1"};
         //              am.invokeMethod("createRecord", sn1);
           }
          
          
      /**=======================================================================
         *            CREATE ADD DETAIL TO SHIPMENT
         * ====================================================================**/ 
        if(pageContext.getParameter(EVENT_PARAM).equals("ADDALLOCATION"))
          {
//              OADBTransaction txn = am.getOADBTransaction();
//              txn.rollback();
             pageContext.writeDiagnostics(this, "Inside ADDALLOCATION Logic ----------------------->",4);
              System.out.println("Inside ADDALLOCATION Logic ----------------------->");
            
                
   
                  pageContext.writeDiagnostics(this, "Inside ADDALLOCATION Logic ----------------------->",4);
                  System.out.println("Inside ADDALLOCATION Logic ----------------------->");
                 
                  int fetchedRowCount1 = dtlVO.getFetchedRowCount();
              pageContext.writeDiagnostics(this, "Inside ADDALLOCATION Logic Get Fetched Row Count ----------------------->"+fetchedRowCount1,4);
                  System.out.println("Value Of Fetched Row Count "+fetchedRowCount1);
                  RowSetIterator rowsetiterator = dtlVO.createRowSetIterator("HdrIter");
                      System.out.println("Vo Executed My add allocation Ieteradfjkhnjnjshfuhj"+rowsetiterator);
                      if (fetchedRowCount1 > 0)
                      {
                          rowsetiterator.setRangeStart(0); //Assiging the Range
                          rowsetiterator.setRangeSize(fetchedRowCount1);
                          for (int i = 0; i < fetchedRowCount1; i++)
                          {
              
                              OARow row1 = (OARow)rowsetiterator.getRowAtRangeIndex(i); //Iterating and passing values for each row to get printed
                              pageContext.writeDiagnostics(this, "Obtaining the from Row Set Iterator"+row1, 4);
                             
                              if(row1.getAttribute("SelectFlag")!=null&&row1.getAttribute("SelectFlag").equals("Y"))
                              {
                              String selrow = (row1.getAttribute("SelectFlag")).toString();// Getting the Value of Each Attributes
                              pageContext.writeDiagnostics(this, "Obtaining the from Row Set Iterator"+selrow, 4);
                                  counter = counter+1;
                                      Serializable[] sn1={"shipSoEO1"};
                                      am.invokeMethod("createRecordSo", sn1); 
                                      soVO.getCurrentRow().setAttribute("ShipmtHdrId", row1.getAttribute("ShipmtHdrId"));
            
                                      soVO.getCurrentRow().setAttribute("ShipmtDtlId", row1.getAttribute("ShipmtDtlId")); 
                                  pageContext.writeDiagnostics(this, "Obtaining the ShipmtDtlId for Creating and Setting Row"+row1.getAttribute("ShipmtDtlId"), 4);
                                      soVO.getCurrentRow().setAttribute("SalesOrderNumber", row1.getAttribute("SalesOrderNumber")); 
                                  pageContext.writeDiagnostics(this, "Obtaining the ShipmtDtlId for Creating and Setting Row"+row1.getAttribute("SalesOrderNumber"), 4);
                              }
                                  
                              }
                             if(counter == 0)
                             {
                                 System.out.println("Inside counter == 0 and so throwing Exception");
                                 OAException confirmMessage = new OAException("Select a Shipment and a Detail Number", OAException.ERROR);     
                                 pageContext.putDialogMessage(confirmMessage);
                             }
                            
              
                          }
                     
          }
          
      /**=======================================================================
         *            CREATE ADD SALE ORDER TO SHIPMENT
         * ====================================================================**/ 
        if(pageContext.getParameter(EVENT_PARAM).equals("ADDDETAILTOSHIPMENT")) 
          {
              pageContext.writeDiagnostics(this, "Inside ADDDETAILTOSHIPMENT Logic ----------------------->",4);
              System.out.println("Inside ADDDETAILTOSHIPMENT Logic ----------------------->");
              System.out.println("Inside ADDDETAILTOSHIPMENT Logic ------------hvo.getCurrentRow().getAttribute(\"SEL\")----------->"+hvo.getCurrentRow().getAttribute("SEL"));
              int fetchedRowCount132 = hvo.getFetchedRowCount();
              System.out.println("Value Of Fetched Row Count "+fetchedRowCount132);
              RowSetIterator rowsetiterator = hvo.createRowSetIterator("HdrIter");
                  System.out.println("Vo Executed My Delete Ieteradfjkhnjnjshfuhj"+rowsetiterator);
                  if (fetchedRowCount132 > 0)
                  {
                      rowsetiterator.setRangeStart(0); //Assiging the Range
                      rowsetiterator.setRangeSize(fetchedRowCount132);
                      for (int i = 0; i < fetchedRowCount132; i++)
                      {
    
                          OARow row1 = (OARow)rowsetiterator.getRowAtRangeIndex(i); //Iterating and passing values for each row to get printed
                          System.out.println("Obtaining the from Row Set Iterator"+row1);
                         
                          if(row1.getAttribute("SEL")!=null)
                          {
                          String selrow = (row1.getAttribute("SEL")).toString();// Getting the Value of Each Attributes
                           System.out.println("Obtaining the from Row Set Iterator"+selrow);
                              counter = counter+1;
                             
                                  System.out.println("Something Happens over here"+row1.getAttribute("ShipmtHdrId")); //checking the Doc Id for Verification whether correct row gets fetched
                                 HashMap hm=new HashMap();  //Tried to send parameter through SetFowardURL
                                  hm.put("SHIPMTHDRID", row1.getAttribute("ShipmtHdrId"));
                                  hm.put("POVENDORID", row1.getAttribute("VendorId"));
                              
                              
                                //Parameters has been set in Image Item.. Look up for Reference
                                   pageContext.setForwardURL("OA.jsp?page=/xxcus/oracle/apps/po/shipment/webui/AddDtlsPG",
                                   null,
                                   //OAWebBeanConstants.KEEP_NO_DISPLAY_MENU_CONTEXT,
                                    OAWebBeanConstants.KEEP_MENU_CONTEXT,
                                   null,
                                   hm, //HashMap
                                   
                                   true,
                                   OAWebBeanConstants.ADD_BREAD_CRUMB_YES,
                                   OAWebBeanConstants.IGNORE_MESSAGES);                       
                              }
                              
                          }
                         if(counter == 0)
                         {
                             System.out.println("Inside counter == 0 and so throwing Exception");
                             OAException confirmMessage = new OAException("Select a Shipment Number", OAException.ERROR);     
                             pageContext.putDialogMessage(confirmMessage);
                         }
                        
//                          System.out.println("Obtaining the from Row Set Iterator "+selrow); 
//                          Number a=row.getBalance();
//                          int b=row.getAttributeCount();
//                          System.out.println(�Value of Account Number�+primaryKey+� Value of Balance�+a+�Value of Attribute Count �+b);
//                          if (primaryKey.compareTo(dummyAccNo) == 0)
//                          {
//                          System.out.println(�Values Mh�+primaryKey);
//                          break; // only one possible selected row in this case
//                          }
    
                      }
                 
              
//              System.out.println("Event named update "+EVENT_PARAM+SOURCE_PARAM);
//                                 String rowReference = pageContext.getParameter(OAWebBeanConstants.EVENT_SOURCE_ROW_REFERENCE);    //Obtaining the Row Reference of the current Row
//                                 Serializable[] s={rowReference};//    Sending the parameter thro a Serializable parameter
//                                 OARow row = (OARow)am.findRowByRef(rowReference); //Getting the rowRefence and getting as a row
//                                 System.out.println("Value of The Rwo"+row);   //Checking Value of the Row
                                 
                             
                
//              Serializable[] sn1={"shipDtlVO1"};
//              am.invokeMethod("createRecordDtl", sn1); 
     
            }
            
            
         /**=======================================================================
            *            SUBMIT ON ADDSHIPMENT 
            * ====================================================================**/ 
           if(pageContext.getParameter(EVENT_PARAM).equals("SAVEANDCONFIRM"))
             {
//                 pageContext.writeDiagnostics(this, "Inside SAVE Logic ----------------------->",4);
//                 System.out.println("Inside SAVE Logic ----------------------->");
//                 am.invokeMethod("apply"); 
//                 OAException confirmMessage = new OAException("The Transactions has been saved Successfully", OAException.CONFIRMATION);     
//                 System.out.println("Inside SAVE Logic Before throwing Exception ----------------------->");
//                 pageContext.putDialogMessage(confirmMessage);
                 
                 pageContext.writeDiagnostics(this, "Inside SAVEANDCONFIRM Logic ----------------------->",4);
                 System.out.println("Inside SAVEANDCONFIRM Logic ----------------------->");
                 System.out.println("Inside SAVEANDCONFIRM Logic ------------hvo.getCurrentRow().getAttribute(\"SEL\")----------->"+hvo.getCurrentRow().getAttribute("SEL"));
                 int fetchedRowCount132 = hvo.getFetchedRowCount();
                 System.out.println("Value Of Fetched Row Count "+fetchedRowCount132);
                 RowSetIterator rowsetiterator = hvo.createRowSetIterator("HdrIter");
                     if (fetchedRowCount132 > 0)
                     {
                         rowsetiterator.setRangeStart(0); //Assiging the Range
                         rowsetiterator.setRangeSize(fetchedRowCount132);
                         for (int i = 0; i < fetchedRowCount132; i++)
                         {
                              OARow row1 = (OARow)rowsetiterator.getRowAtRangeIndex(i); //Iterating and passing values for each row to get printed
                             System.out.println("Obtaining the from Row Set Iterator"+row1);
                            
                             if(row1.getAttribute("SEL")!=null&&(row1.getAttribute("SEL").toString().equals("Y")))
                             {
                             String selrow = (row1.getAttribute("SEL")).toString();// Getting the Value of Each Attributes
                              System.out.println("Obtaining the from Row Set Iterator"+selrow);
                                 counterS = counterS+1;
                                     row1.setAttribute("SysemStatus", "Confirmed");
                                     am.invokeMethod("apply");  
 
                                     OAException confirmMessage = new OAException("Selected Shipment Number : "+(row1.getAttribute("ShipmentNumber").toString())+" has been changed to Confirmed", OAException.CONFIRMATION);     
                                     pageContext.putDialogMessage(confirmMessage);
                                 }
                                 
                             }
                            if(counterS == 0)
                            {
                                System.out.println("Inside counterS == 0 and so throwing Exception");
                                OAException confirmMessage = new OAException("Select a Shipment Number", OAException.ERROR);     
                                pageContext.putDialogMessage(confirmMessage);
                            }
                            
                           
                 //                          System.out.println("Obtaining the from Row Set Iterator "+selrow);
                 //                          Number a=row.getBalance();
                 //                          int b=row.getAttributeCount();
                 //                          System.out.println(�Value of Account Number�+primaryKey+� Value of Balance�+a+�Value of Attribute Count �+b);
                 //                          if (primaryKey.compareTo(dummyAccNo) == 0)
                 //                          {
                 //                          System.out.println(�Values Mh�+primaryKey);
                 //                          break; // only one possible selected row in this case
                 //                          }
                 
                         }
                    
                 
                 //              System.out.println("Event named update "+EVENT_PARAM+SOURCE_PARAM);
                 //                                 String rowReference = pageContext.getParameter(OAWebBeanConstants.EVENT_SOURCE_ROW_REFERENCE);    //Obtaining the Row Reference of the current Row
                 //                                 Serializable[] s={rowReference};//    Sending the parameter thro a Serializable parameter
                 //                                 OARow row = (OARow)am.findRowByRef(rowReference); //Getting the rowRefence and getting as a row
                 //                                 System.out.println("Value of The Rwo"+row);   //Checking Value of the Row
                                    
                                
                 
                 
                 
             }
             
             
                  /**=======================================================================
                     *            CREATE ADD SALE ORDER TO SHIPMENT
                     * ====================================================================**/ 
                    if(pageContext.getParameter(EVENT_PARAM).equals("CANCEL")) 
                      {
                          pageContext.writeDiagnostics(this, "Inside CANCEL Logic ----------------------->",4);
                          System.out.println("Inside CANCEL Logic ----------------------->");
                          System.out.println("Inside CANCEL Logic ------------hvo.getCurrentRow().getAttribute(\"SEL\")----------->"+hvo.getCurrentRow().getAttribute("SEL"));
                          int fetchedRowCount132 = hvo.getFetchedRowCount();
                          System.out.println("Value Of Fetched Row Count "+fetchedRowCount132);
                          RowSetIterator rowsetiterator = hvo.createRowSetIterator("HdrIter");
                              System.out.println("Vo Executed My Delete Ieteradfjkhnjnjshfuhj"+rowsetiterator);
                              if (fetchedRowCount132 > 0)
                              {
                                  rowsetiterator.setRangeStart(0); //Assiging the Range
                                  rowsetiterator.setRangeSize(fetchedRowCount132);
                                  for (int i = 0; i < fetchedRowCount132; i++)
                                  {
                  
                                      OARow row1 = (OARow)rowsetiterator.getRowAtRangeIndex(i); //Iterating and passing values for each row to get printed
                                      System.out.println("Obtaining the from Row Set Iterator"+row1);
                                     
                                      if(row1.getAttribute("SEL")!=null&&(row1.getAttribute("SEL").toString().equals("Y")))
                                      {
                                              OAException mainMessage = new OAException("This SalesOrderNumber "+row1.getAttribute("ShipmentNumber")+" will be Cancelled", OAException.CONFIRMATION); 
                                              OADialogPage dialogPage = new OADialogPage(OAException.WARNING,mainMessage, null, "", "");

                                              String yes = pageContext.getMessage("AK", "FWK_TBX_T_YES", null);
                                              String no = pageContext.getMessage("AK", "FWK_TBX_T_NO", null);
                                              dialogPage.setOkButtonItemName("CancelYesButton");
                                              dialogPage.setOkButtonToPost(true);
                                              dialogPage.setNoButtonToPost(true);
                                              dialogPage.setPostToCallingPage(true);
                                              dialogPage.setOkButtonLabel(yes);                    
                                              dialogPage.setNoButtonLabel(no);
                                              java.util.Hashtable formParams = new java.util.Hashtable(1);
                                              
                                              formParams.put("SHIPMTHDRIDCANCEL", (row1.getAttribute("ShipmtHdrId").toString()));
                                              
                                              dialogPage.setFormParameters(formParams);
                                                pageContext.writeDiagnostics(this, "Before Redirecting to Dialog Page", 4);
                                              pageContext.redirectToDialogPage(dialogPage);
                                      
                                      String selrow = (row1.getAttribute("SEL")).toString();// Getting the Value of Each Attributes
                                       System.out.println("Obtaining the from Row Set Iterator"+selrow);
                                          counter = counter+1;
                                         
//                                              System.out.println("Something Happens over here"+row1.getAttribute("ShipmtHdrId")); //checking the Doc Id for Verification whether correct row gets fetched
//                                              row1.setAttribute("SysemStatus", "Cancelled");   
//                                              am.invokeMethod("apply");  
//                                               
//                                              OAException confirmMessage = new OAException("Selected Shipment Number : "+(row1.getAttribute("ShipmentNumber").toString())+" has been changed to Cancelled Status", OAException.CONFIRMATION);     
//                                              pageContext.putDialogMessage(confirmMessage);
                                          }
                                          
                                      }
                                     if(counter == 0)
                                     {
                                         System.out.println("Inside counter == 0 and so throwing Exception");
                                         OAException confirmMessage = new OAException("Select a Shipment Number", OAException.ERROR);     
                                         pageContext.putDialogMessage(confirmMessage);
                                     }
                                    
                  //                          System.out.println("Obtaining the from Row Set Iterator "+selrow);
                  //                          Number a=row.getBalance();
                  //                          int b=row.getAttributeCount();
                  //                          System.out.println(�Value of Account Number�+primaryKey+� Value of Balance�+a+�Value of Attribute Count �+b);
                  //                          if (primaryKey.compareTo(dummyAccNo) == 0)
                  //                          {
                  //                          System.out.println(�Values Mh�+primaryKey);
                  //                          break; // only one possible selected row in this case
                  //                          }
                  
                                  }
                             
                          
                  //              System.out.println("Event named update "+EVENT_PARAM+SOURCE_PARAM);
                  //                                 String rowReference = pageContext.getParameter(OAWebBeanConstants.EVENT_SOURCE_ROW_REFERENCE);    //Obtaining the Row Reference of the current Row
                  //                                 Serializable[] s={rowReference};//    Sending the parameter thro a Serializable parameter
                  //                                 OARow row = (OARow)am.findRowByRef(rowReference); //Getting the rowRefence and getting as a row
                  //                                 System.out.println("Value of The Rwo"+row);   //Checking Value of the Row
                                             
                                         
                            
                  //              Serializable[] sn1={"shipDtlVO1"};
                  //              am.invokeMethod("createRecordDtl", sn1);
                  
                        }
                        
                   if (pageContext.getParameter("CancelYesButton") != null)
                   {
                   
//                      System.out.println("Something Happens over here"+row1.getAttribute("ShipmtHdrId")); //checking the Doc Id for Verification whether correct row gets fetched
//                      row1.setAttribute("SysemStatus", "Cancelled");   
//                      am.invokeMethod("apply");  
//                       
//                     
                   //                   String docNumber = pageContext.getParameter("docNum");
                    pageContext.writeDiagnostics(this, "Inside pageContext.getParameter(\"CancelYesButton\") != null", 4);
                   //                   String docuName = pageContext.getParameter("docName");
                   //                       System.out.println("Doc   name"+docuName);
                   //                   String versionNo = pageContext.getParameter("versNo");
                   String ShipmtHdrIdCanc = pageContext.getParameter("SHIPMTHDRIDCANCEL");
                   //                       System.out.println("Doc version No"+versionNo);
                   
                    try
                      {
                              pageContext.writeDiagnostics(this, "Inside pageContext.getParameter(\"DeleteYesButton\") != null and Try Block"+ShipmtHdrIdCanc, 4);
 
                              int fetchedRowCount = hvo.getFetchedRowCount();                
                              pageContext.writeDiagnostics(this, "Inside Cancel Functionality Fetched Row Count"+fetchedRowCount, 4);
                              RowSetIterator cancelIter = hvo.createRowSetIterator("CancelIter");
                              pageContext.writeDiagnostics(this, "Checking for Cancel and  Iteration   -- > "+cancelIter, 4);
                              if (fetchedRowCount > 0)
                              {
                              cancelIter.setRangeStart(0);
                              cancelIter.setRangeSize(fetchedRowCount);
                              for (int i = 0; i < fetchedRowCount; i++)
                              {
                                    pageContext.writeDiagnostics(this, "Inside For Loop Cancellation  -- > "+i, 4);
                                  OARow row = (shipmtHdrVORowImpl)cancelIter.getRowAtRangeIndex(i);
                      
                   //                                      String primaryKey = (row.getAttribute("SalesOrderNumber")).toString();
                   //                                      String primaryKey1 =(row.getAttribute("PoLineNumber")).toString();
                                   String ShipmtHdrIdPrim =(row.getAttribute("ShipmtHdrId")).toString();
                                   pageContext.writeDiagnostics(this, "Inside DeleteYesButton is not null and getting ShipmtDtlIdPrim"+ShipmtHdrIdPrim+" Value of i"+i, 4);
                                  
                                   if ((ShipmtHdrIdCanc.compareTo(ShipmtHdrIdPrim) == 0))
                                   {
                                       // This performs the actual delete.
                                       pageContext.writeDiagnostics(this, "Getting the Values ShipmtDtlIdPrim for HDR Cancel ShipmtDtlIdPrim: "+ShipmtHdrIdPrim+" ShipmtDtlId  "+ShipmtHdrIdCanc, 4);
                                       row.setAttribute("SysemStatus", "Cancelled");   
                                       //                     
                                       am.getTransaction().commit();        
                                          hvo.setWhereClause("SHIPMT_HDR_ID = "+(row.getAttribute("ShipmtHdrId").toString()));                            
                                          hvo.executeQuery();
                                          hvo.first();
                                          pageContext.writeDiagnostics(this, "Getting the Header Query after Cancellation ------------->  "+hvo.getQuery(), 4); 
                                          
                                       OAException confirmMessage = new OAException("Selected Shipment Number : "+(row.getAttribute("SysemStatus").toString())+" has been changed to Cancelled Status", OAException.CONFIRMATION);     
                                       pageContext.putDialogMessage(confirmMessage);
                                        
                                          
                                       //   break; // only one possible selected row in this case
                                      }

                                }
                                }
                                        cancelIter.closeRowSetIterator();
                              
                      }
                      catch(Exception e)
                      {
                          pageContext.writeDiagnostics(this, "Exception Occured "+e, 4);
                      }
                   }
             
             
      /**=======================================================================
         *            SUBMIT ON ADDSHIPMENT 
         * ====================================================================**/ 
        if(pageContext.getParameter(EVENT_PARAM).equals("SAVE"))
          {
              pageContext.writeDiagnostics(this, "Inside SAVE Logic ----------------------->",4);
              System.out.println("Inside SAVE Logic ----------------------->"+soVO.getRowCount());
              int rowcount2 = dtlVO.getRowCount();
              int rowcount1 = soVO.getRowCount();
              if( rowcount1 == 0 &&rowcount2 == 0)    
              {
//              am.invokeMethod("apply");
              pageContext.writeDiagnostics(this, "Inside SAVE Logic Rowcount = 0 ----------------------->"+rowcount1,4);
//              System.out.println("Inside SAVE Logic Before throwing Exception ----------------------->");
//              pageContext.putDialogMessage(confirmMessage);
              }
              else if( rowcount1 == 0 && rowcount2 > 0)
              {
                  pageContext.writeDiagnostics(this, "rowcount1 == 0 && rowcount2 > 0 Condition" , 4);
                  OAException confirmMessage = new OAException("Records cannot be saved as Sales Order Subsection cannot be Empty ", OAException.ERROR);     
                  System.out.println("Inside SAVE Logic Before throwing Exception ----------------------->");
                  pageContext.putDialogMessage(confirmMessage);
              }
              
              else
              {
              
                  Row rowa[] = dtlVO.getAllRowsInRange();
//                  pageContext.writeDiagnostics(this, "Getting Current row of SoVO"+soVO.getCurrentRow().getAttribute("SalesOrderNumber"), 4);
//                  pageContext.writeDiagnostics(this, "Getting Current row of dtlVO"+dtlVO.getCurrentRow().getAttribute("ShipmtDtlId"), 4);
                  //                  Row row[] = soVO.getFilteredRows();
                  System.out.println("Proceed in SAVE       --------"+rowa.length);
                  
                  
                    
                   for (int j=0;j<rowa.length;j++)
                     {
                         OARow rowaa = (OARow)rowa[j];
              
//                  Row row[] = soVO.getAllRowsInRange(rowi.);
                        soVO.setWhereClause(null);                          //TO Reset the VO and to Recheck the Intended Functionality does work
                        soVO.setWhereClauseParams(null);
                         soVO.setWhereClause("SHIPMT_DTL_ID = "+(rowaa.getAttribute("ShipmtDtlId")).toString()); 
                         soVO.executeQuery();
                         soVO.first();
                         pageContext.writeDiagnostics(this, "Executing the SVO Query and then Resetting  to get the Query: "+soVO.getQuery()+" soVO.getRowCount() "+soVO.getRowCount()+" soVO.getFetchedRowCount() "+soVO.getFetchedRowCount(), 4);
                    Row row[] = soVO.getFilteredRows("ShipmtDtlId", rowaa.getAttribute("ShipmtDtlId"));            //Approach not working    
                   
                     
                    pageContext.writeDiagnostics(this, "Getting Current row of dtlVO"+rowaa.getAttribute("ShipmtDtlId"), 4);
                         pageContext.writeDiagnostics(this, "Getting Current row of SoVO-------->"+row.length, 4);
                  
//                  Row row[] = soVO.getFilteredRows();
//                  System.out.println("Proceed in SAVE       --------"+row.length);
                    //     int counter1=0;
                     Integer getSumOfAllQty=0;
                         List<Integer> list = new ArrayList<Integer>();  
                    
                         
                         
                   for (int i=0;  i<row.length; i++)
                     {
                         OARow rowi = (OARow)row[i];
                         pageContext.writeDiagnostics(this, "Getting Current row of SoVO"+i+"ShipmtSoId ---> "+rowi.getAttribute("ShipmtSoId")+"rowi.getAttribute(\"AllocationQty\")"+rowi.getAttribute("AllocationQty"), 4);
                         getSumOfAllQty = getSumOfAllQty+Integer.parseInt((rowi.getAttribute("AllocationQty")).toString());
                         
                         list.add((Integer)rowi.getAttribute("AllocationQty"));
                        
                         pageContext.writeDiagnostics(this, "Getting Current row of getSumOfAllQty Value to Check the Values "+getSumOfAllQty, 4);
                     }
                         pageContext.writeDiagnostics(this, "Outer - Getting Current row of getSumOfAllQty Value to Check the Values "+getSumOfAllQty, 4);
                     if(getSumOfAllQty!=0)
                     {
                        if( Integer.parseInt((rowaa.getAttribute("AllocationQty")).toString()) == getSumOfAllQty)
                        {
                            am.invokeMethod("apply"); 
                            pageContext.writeDiagnostics(this, "Condition is Right    getSumOfAllQty"+getSumOfAllQty+"Integer.parseInt((rowaa.getAttribute(\"AllocationQty\")).toString())"+Integer.parseInt((rowaa.getAttribute("AllocationQty")).toString()), 4);
                            OAException confirmMessage = new OAException("The Transactions has been saved Successfully", OAException.CONFIRMATION);     
                            System.out.println("Inside SAVE Logic Before throwing Exception ----------------------->");
                            pageContext.putDialogMessage(confirmMessage);
                        }
                        else
                        {
                            pageContext.writeDiagnostics(this, "Ensure Allocation Quantity has to mh with Shipment Detail    getSumOfAllQty"+getSumOfAllQty+"Integer.parseInt((rowaa.getAttribute(\"AllocationQty\")).toString())"+Integer.parseInt((rowaa.getAttribute("AllocationQty")).toString()), 4);
                            OAException confirmMessage = new OAException("Ensure Allocation Quantity has to mh with Shipment Detail", OAException.ERROR);     
                            System.out.println("Inside SAVE Logic Before throwing Exception ----------------------->");
                            pageContext.putDialogMessage(confirmMessage);
                        }
                     }
                                   
                     }
                   
              }
       
          }
          
      /**=======================================================================
         *            DELETE DETAILS
         * ====================================================================**/ 
        if(pageContext.getParameter(EVENT_PARAM).equals("DELETEDETAIL"))
          {
              pageContext.writeDiagnostics(this, "Inside DELETEDETAIL Logic ----------------------->",4);
                String rowReference = pageContext.getParameter(OAWebBeanConstants.EVENT_SOURCE_ROW_REFERENCE);    //Obtaining the Row Reference of the current Row
                OARow row = (OARow)am.findRowByRef(rowReference);
              pageContext.writeDiagnostics(this, " row.getAttribute(\"ShipmtDtlId\")"+ row.getAttribute("ShipmtDtlId")+" row.getAttribute(\"PoLineId\")"+row.getAttribute("PoLineId"), 4);
                    String docNo = " ";
                    if(row.getAttribute("SalesOrderNumber")!=null)
                    {
                         docNo = (row.getAttribute("SalesOrderNumber")).toString();
                    }
                   String docName = (row.getAttribute("PoNumber")).toString();
                   String verNo = (row.getAttribute("PoLineNumber")).toString();
                   String ShipmtDtlId = (row.getAttribute("ShipmtDtlId")).toString();
                   MessageToken[] tokens = { new MessageToken("DOC_NUM", docNo)};
                   System.out.println("Tokens detect ed   efjasdflsjkf"+tokens);

                   OAException mainMessage = new OAException("This SalesOrderNumber "+docNo+" will be Deleted", OAException.CONFIRMATION); 
                   OADialogPage dialogPage = new OADialogPage(OAException.WARNING,mainMessage, null, "", "");

                   String yes = pageContext.getMessage("AK", "FWK_TBX_T_YES", null);
                   String no = pageContext.getMessage("AK", "FWK_TBX_T_NO", null);
                   dialogPage.setOkButtonItemName("DeleteYesButton");
                   dialogPage.setOkButtonToPost(true);
                   dialogPage.setNoButtonToPost(true);
                   dialogPage.setPostToCallingPage(true);
                   dialogPage.setOkButtonLabel(yes);                    
                   dialogPage.setNoButtonLabel(no);
                   java.util.Hashtable formParams = new java.util.Hashtable(1);
               
                   formParams.put("docNum", docNo);
                   formParams.put("versNo", verNo);
                   formParams.put("ShipmtDtlId", ShipmtDtlId);
                   dialogPage.setFormParameters(formParams);
                     pageContext.writeDiagnostics(this, "Before Redirecting to Dialog Page", 4);
                   pageContext.redirectToDialogPage(dialogPage);
          }
                   
                  
      if (pageContext.getParameter("DeleteYesButton") != null)
      {
      //                   String docNumber = pageContext.getParameter("docNum");
       pageContext.writeDiagnostics(this, "Inside pageContext.getParameter(\"DeleteYesButton\") != null", 4);
      //                   String docuName = pageContext.getParameter("docName");
      //                       System.out.println("Doc   name"+docuName);
      //                   String versionNo = pageContext.getParameter("versNo");
      String ShipmtDtlId = pageContext.getParameter("ShipmtDtlId");
      //                       System.out.println("Doc version No"+versionNo);
      
       try
         {
                 pageContext.writeDiagnostics(this, "Inside pageContext.getParameter(\"DeleteYesButton\") != null and Try Block", 4);
                 shipDtlVORowImpl delrow = null;
                 int fetchedRowCount = dtlVO.getFetchedRowCount();                
                 pageContext.writeDiagnostics(this, "Inside Delete Functionality Fetched Row Count"+fetchedRowCount, 4);
                 RowSetIterator deleteIter = dtlVO.createRowSetIterator("deleteIter");
                 pageContext.writeDiagnostics(this, "Checking for Delete Iteration   -- > "+deleteIter, 4);
                 if (fetchedRowCount > 0)
                 {
                 deleteIter.setRangeStart(0);
                 deleteIter.setRangeSize(fetchedRowCount);
                 for (int i = 0; i < fetchedRowCount; i++)
                 {
                     OARow row = (shipDtlVORowImpl)deleteIter.getRowAtRangeIndex(i);
         
      //                                      String primaryKey = (row.getAttribute("SalesOrderNumber")).toString();
      //                                      String primaryKey1 =(row.getAttribute("PoLineNumber")).toString();
                      String ShipmtDtlIdPrim =(row.getAttribute("ShipmtDtlId")).toString();
                      pageContext.writeDiagnostics(this, "Inside DeleteYesButton is not null and getting ShipmtDtlIdPrim"+ShipmtDtlIdPrim+" Value of i"+i, 4);
                     
                      if ((ShipmtDtlId.compareTo(ShipmtDtlIdPrim) == 0))
                      {
                          // This performs the actual delete.
                          pageContext.writeDiagnostics(this, "Getting the Values ShipmtDtlIdPrim for DTL Deletion ShipmtDtlIdPrim: "+ShipmtDtlIdPrim+" ShipmtDtlId  "+ShipmtDtlId, 4);
                          row.remove();
                          am.getTransaction().commit();
                          
                          
                          //Deleting the SO Rows   Start -----------------------------------------------------------------
                          int fetchedRowCountSoVo = soVO.getFetchedRowCount();
                          RowSetIterator deleteIterSOVO = soVO.createRowSetIterator("deleteIterSOVO");
                          if (fetchedRowCountSoVo > 0)
                          {
                          deleteIterSOVO.setRangeStart(0);
                          deleteIterSOVO.setRangeSize(fetchedRowCountSoVo);
                          for (int j = 0; j < fetchedRowCountSoVo; j++)
                          {
                          OARow rowSO = (shipSoEORowImpl)deleteIterSOVO.getRowAtRangeIndex(j);

                              String ShipmtDtlIdSOPrim =(rowSO.getAttribute("ShipmtDtlId")).toString();
                           if ((ShipmtDtlId.compareTo(ShipmtDtlIdSOPrim) == 0))
                           {
                           // This performs the actual delete.
                           System.out.println("Getting the Values SO Level ShipmtDtlIdPrim "+ShipmtDtlIdPrim+" ShipmtDtlId  "+ShipmtDtlId);
                           pageContext.writeDiagnostics(this, "Getting the Values on SO Level ShipmtDtlIdPrim "+ShipmtDtlIdPrim+" ShipmtDtlId  "+ShipmtDtlId, 4);
                           rowSO.remove();
                          
                           am.getTransaction().commit();
                           break; // only one possible selected row in this case
                           }

                          }
                          }
                          deleteIter.closeRowSetIterator();
                          deleteIterSOVO.closeRowSetIterator();
                          
                          //Deleting the SO Rows   End -----------------------------------------------------------------
                          
                          break; // only one possible selected row in this case
                      }

                 }
                 }
                 
                   
         }
         catch(Exception e)
         {
                 System.out.println("Exception Occured in Delete logic AM"+e);
         }
      }
      
        /**=======================================================================
         *            DELETE SO - INDIVIDUAL SO
         * ====================================================================**/ 
           if(pageContext.getParameter(EVENT_PARAM).equals("DELETESO"))
               {
                      System.out.println("Inside DELETESO Logic ----------------------->");
                      String rowReference         =           pageContext.getParameter(OAWebBeanConstants.EVENT_SOURCE_ROW_REFERENCE);    //Obtaining the Row Reference of the current Row
                      System.out.println("Row Ref"+rowReference);         
                      OARow row                   =           (OARow)am.findRowByRef(rowReference);
                      String ShipmtSoId           =           (row.getAttribute("ShipmtSoId")).toString();
                      String SalesOrderNumber           =           (row.getAttribute("SalesOrderNumber")).toString();
                      OAException mainMessage     =           new OAException("This SalesOrderNumber "+SalesOrderNumber+" will be Deleted", OAException.CONFIRMATION); 
                      OADialogPage dialogPage     =           new OADialogPage(OAException.WARNING,mainMessage, null, "", "");
                     String yes                   =           pageContext.getMessage("AK", "FWK_TBX_T_YES", null);
                     String no                    =           pageContext.getMessage("AK", "FWK_TBX_T_NO", null);
                     dialogPage.setOkButtonItemName("DeleteYesButtonS");
                     dialogPage.setOkButtonToPost(true);
                     dialogPage.setNoButtonToPost(true);
                     dialogPage.setPostToCallingPage(true);
                     dialogPage.setOkButtonLabel(yes);
                     dialogPage.setNoButtonLabel(no);
                     java.util.Hashtable formParams = new java.util.Hashtable(1);
                     formParams.put("ShipmtSoId", ShipmtSoId);
                     dialogPage.setFormParameters(formParams);
                     pageContext.writeDiagnostics(this, "Before Redirecting to Dialog Page", 4);
                     pageContext.redirectToDialogPage(dialogPage);
          }
 
         if (pageContext.getParameter("DeleteYesButtonS") != null)
          {
                     String ShipmtSoId = pageContext.getParameter("ShipmtSoId");
                     System.out.println("ShipmtSoId   --- > "+ShipmtSoId);
                    Serializable[] parameters = { ShipmtSoId};
                    try
                      {
                              pageContext.writeDiagnostics(this, "Inside pageContext.getParameter(\"DeleteYesButton\") != null and Try Block", 4);
                              shipSoEORowImpl delrow = null;
                              int fetchedRowCount = soVO.getFetchedRowCount();
                              System.out.println("Fetched Row Cound"+fetchedRowCount);
                              RowSetIterator deleteIter = soVO.createRowSetIterator("deleteIter");
                              System.out.println("deleteIter"+deleteIter);
                              pageContext.writeDiagnostics(this, "Checking for Delete Iteration   -- > "+deleteIter, 4);
                              if (fetchedRowCount > 0)
                                {
                                  deleteIter.setRangeStart(0);
                                  deleteIter.setRangeSize(fetchedRowCount);
                                      for (int i = 0; i < fetchedRowCount; i++)
                                          {
                                              pageContext.writeDiagnostics(this, "Inside For Loop ---------->", 4);
                                          OARow row = (shipSoEORowImpl)deleteIter.getRowAtRangeIndex(i);
                                          String ShipmtSoIdPrim =(row.getAttribute("ShipmtSoId")).toString();
                                              pageContext.writeDiagnostics(this, "Getting the Values ShipmtSoIdPrim "+ShipmtSoIdPrim+" ShipmtSoId  "+ShipmtSoId, 4);
                                              if ((ShipmtSoId.compareTo(ShipmtSoIdPrim) == 0))
                                                   {
                                                        System.out.println("Getting the Values ShipmtSoIdPrim "+ShipmtSoIdPrim+" ShipmtSoId  "+ShipmtSoId);
                                                        pageContext.writeDiagnostics(this, "Getting the Values ShipmtSoIdPrim "+ShipmtSoIdPrim+" ShipmtSoId  "+ShipmtSoId, 4);
                                                        row.remove();
                                                        am.getTransaction().commit();
                                                        break; 
                                                   }
                                    
                                          }
            
                                  }
                            deleteIter.closeRowSetIterator();
                       
                       }
                     catch(Exception e)
                       {
                              System.out.println("e  "+e);
                       }
                     
                   }
      

          

      /**=======================================================================
         *           Search when Find Button Clicked
         * ====================================================================**/ 
                 
  
              if (pageContext.getParameter(EVENT_PARAM).equals("SEARCH"))
              {
                pageContext.writeDiagnostics(this, "Inside Search Logic ----------------------->",4);
                System.out.println("Inside Search Logic ----------------------->");                   
                Enumeration x=null;
                 x=pageContext.getParameterNames();
                          if(x!=null)
                          
                          {
                
                          System.out.println("Elements in Enum"+x);
                          while(x.hasMoreElements())
                          {
                          String aParamName = (String)x.nextElement();
                          System.out.println("Obatining the Child Names     : "+aParamName);
                          }
                          System.out.println("VO Gets Queried");
                
                          }
                 
                 
                System.out.println("pageContext.getParameter(\"ShipmentNumber\")"+pageContext.getParameter("ShipmentNumber"));
                System.out.println("pageContext.getParameter(\"SupplierName\")"+pageContext.getParameter("SupplierName"));
                System.out.println("pageContext.getParameter(\"SupplierInvoice\")"+pageContext.getParameter("SupplierInvoice"));
                System.out.println("pageContext.getParameter(\"AwbNumber\")"+pageContext.getParameter("AwbNumber"));
                System.out.println("pageContext.getParameter(\"CreationDate\")"+pageContext.getParameter("CreationDate"));
                System.out.println("pageContext.getParameter(\"Status\")"+pageContext.getParameter("Status"));
                
                pageContext.writeDiagnostics(this, "pageContext.getParameter(\"ShipmentNumber\")"+pageContext.getParameter("ShipmentNumber") , 4);
                pageContext.writeDiagnostics(this, "pageContext.getParameter(\"SupplierName\")"+pageContext.getParameter("SupplierName"), 4);
                pageContext.writeDiagnostics(this, "pageContext.getParameter(\"SupplierInvoice\")"+pageContext.getParameter("SupplierInvoice"), 4);
                pageContext.writeDiagnostics(this, "pageContext.getParameter(\"AwbNumber\")"+pageContext.getParameter("AwbNumber"), 4);
                pageContext.writeDiagnostics(this, "pageContext.getParameter(\"CreationDate\")"+pageContext.getParameter("CreationDate"), 4);
                pageContext.writeDiagnostics(this, "pageContext.getParameter(\"SystemStatus\")"+pageContext.getParameter("SystemStatus"), 4);
                pageContext.writeDiagnostics(this, "pageContext.getParameter(\"BMEStatus\")"+pageContext.getParameter("BMEStatus"), 4);
                
                List whClauseParams = new ArrayList();
                if (pageContext.getParameter("ShipmentNumber") != null && pageContext.getParameter("ShipmentNumber") != "") { 
                    try
                      {
                     // vo.setWhereClauseParam(0, pageContext.getParameter("DocumentName"));
                         whClauseParams.add(" and QRSLT.SHIPMENT_NUMBER like '%"+pageContext.getParameter("ShipmentNumber")+ "%'");
                      }
                      catch(Exception e){
                      System.out.println(e);
                      }
                }
                
                
                if (pageContext.getParameter("SupplierName") != null && pageContext.getParameter("SupplierName")!="") { 
                    try
                      {
                     // vo.setWhereClauseParam(0, pageContext.getParameter("DocumentName"));
                         whClauseParams.add(" and QRSLT.SUPPLIER_NAME like '%"+pageContext.getParameter("SupplierName")+ "%'");
                      }
                      catch(Exception e){
                      System.out.println(e);
                      }
                }
                if (pageContext.getParameter("SupplierInvoice")!= null && pageContext.getParameter("SupplierInvoice")!="") { 
                    try
                      {
                     // vo.setWhereClauseParam(0, pageContext.getParameter("DocumentName"));
                         whClauseParams.add(" and QRSLT.SUPPLIER_INVOICE like '%"+pageContext.getParameter("SupplierInvoice")+ "%'");
                      }
                      catch(Exception e){
                      System.out.println(e);
                      }
                }
                if (pageContext.getParameter("AwbNumber") != null &&pageContext.getParameter("AwbNumber")!="") { 
                    try
                      {
                     // vo.setWhereClauseParam(0, pageContext.getParameter("DocumentName"));
                         whClauseParams.add(" and QRSLT.AWB_NUMBER like '%"+pageContext.getParameter("AwbNumber")+ "%'");
                      }
                      catch(Exception e){
                      System.out.println(e);
                      }
                }
                
                

                if (pageContext.getParameter("CreationDateFrom") != null && pageContext.getParameter("CreationDateFrom")!="") { 
                    try
                      {
                     // vo.setWhereClauseParam(0, pageContext.getParameter("EffectiveStartDate"));
                         whClauseParams.add(" and trunc(QRSLT.CREATION_DATE) >= to_date('"+pageContext.getParameter("CreationDateFrom")+ "', 'DD-Mon-YYYY')");
                      }
                      catch(Exception e){
                      System.out.println(e);
                      }
                }
                if (pageContext.getParameter("CreationDateTo") != null && pageContext.getParameter("CreationDateTo")!="" ) { 
                    try
                      {
                     // vo.setWhereClauseParam(0, pageContext.getParameter("EffectiveStartDate"));
                      whClauseParams.add(" and trunc(QRSLT.CREATION_DATE) <= to_date('"+pageContext.getParameter("CreationDateTo")+ "', 'DD-Mon-YYYY')");
                      }
                      catch(Exception e){
                      System.out.println(e);
                      }
                }
                if (pageContext.getParameter("SystemStatus") != null &&pageContext.getParameter("SystemStatus")!="") { 
                    try
                      {
                     // vo.setWhereClauseParam(0, pageContext.getParameter("DocumentName"));
                         whClauseParams.add(" and upper(QRSLT.SYSEM_STATUS) like upper('%"+pageContext.getParameter("SystemStatus")+ "%')");
                      }
                      catch(Exception e){
                      System.out.println(e);
                      }
                }
                if (pageContext.getParameter("BMEStatus") != null &&pageContext.getParameter("BMEStatus")!="") { 
                    try
                      {
                     // vo.setWhereClauseParam(0, pageContext.getParameter("DocumentName"));
                         whClauseParams.add(" and upper(QRSLT.STATUS) like upper('%"+pageContext.getParameter("BMEStatus")+ "%')");
                      }
                      catch(Exception e){
                      System.out.println(e);
                      }
                }
                
                  System.out.println("To check VO is not null"+hvo);
                 if(hvo!=null)
                 {
               
                      String sqlparam=" ";
                      if(whClauseParams.isEmpty())
                     {
                                  System.out.println("No Params for Search");
                                 hvo.reset();
                                hvo.setWhereClauseParams(null); // Always reset
                                 hvo.setWhereClause("1=1");                                                             
                                  hvo.executeQuery();    
                                  expVO.reset();
                                  expVO.setWhereClauseParams(null); // Always reset
                                  expVO.setWhereClause("1=1");                                                             
                                   expVO.executeQuery();    
                                
                                  pageContext.writeDiagnostics(this, "Inside Params Empty SAVE - Reset 2 and getting Query ----------------------->"+hvo.getQuery(),4);
                      }
                      else
                      {
                                    for (int i = 0; i < whClauseParams.size(); i++) 
                                    {
                                          String k=whClauseParams.get(i)+"";
                                          System.out.println(whClauseParams.get(i));
                                          sqlparam=sqlparam.concat(k);
                                          System.out.println("The param is "+sqlparam);                                    
                                    }
                 
                                      if((sqlparam!=null)&&(sqlparam!=""))
                                      {
                                          sqlparam=sqlparam.substring(5);
                                          System.out.println("Modified param"+sqlparam);
                                          hvo.reset();
                                          hvo.setWhereClause(sqlparam);                            
                                          hvo.executeQuery();
                                          pageContext.writeDiagnostics(this, "Inside Params with Values "+sqlparam+"SAVE getting Query ----------------------->"+hvo.getQuery(),4);
                                          expVO.reset();
                                          expVO.setWhereClause(sqlparam);                            
                                          expVO.executeQuery();
                                          pageContext.writeDiagnostics(this, "Inside Params with Values "+sqlparam+"SAVE getting Query ----------------------->"+expVO.getQuery(),4);
                                          
                                      }
                           
                        }
                  }
            }
            
      /**=======================================================================
         *           Export Excel Functionality
         * ====================================================================**/ 
      if(pageContext.getParameter(EVENT_PARAM).equals("EXPORT"))
              {
                pageContext.writeDiagnostics(this, "Inside Export Functionality---------------------->", 4);
                String[]  attrList = new String[expVO.getAttributeCount()];
                String[]  attrHdr = new String[expVO.getAttributeCount()];
                for (int i=0; i<=((expVO.getAttributeCount())-1);i++)
                                        {
                                                System.out.println("The columnn Names are "+expVO.getAttributeDef(i).getColumnName());
                                                     attrList[i]=(expVO.getAttributeDef(i).getName()).toString();
                                                     attrHdr[i]=(expVO.getAttributeDef(i).getName()).toString();     //for database column name (vo.getAttributeDef(i).getColumnName()).toString();
                
                                        }
                
                    CommonClass cc = new CommonClass() ;
                    cc.export2Excel(pageContext, "exportShipmtVO1", "excelFile", "MAX", attrList, attrHdr) ; 
                    pageContext.writeDiagnostics(this, "After  calling Export file--------------------->", 4);
                            
              }
              
      /**=======================================================================
         *          On Selecting Radio Button for Headers
         * ====================================================================**/ 
            
        if (pageContext.getParameter(EVENT_PARAM).equals("ROWREF"))
                  {
                         
                    //  System.out.println("Inside the Version Update Event");  
                      pageContext.writeDiagnostics(this, "Inside ROWREF Logic ----------------------->",4);
                      System.out.println("Inside ROWREF Logic ----------------------->");
                      String rowReference = pageContext.getParameter(OAWebBeanConstants.EVENT_SOURCE_ROW_REFERENCE);    //Obtaining the Row Reference of the current Row
                       obtainRow(rowReference,am, pageContext);
                 
   
                 }
      /**=======================================================================
         *          On Selecting Radio Button for Detail
         * ====================================================================**/ 
                 
      if (pageContext.getParameter(EVENT_PARAM).equals("ROWREFDTL"))
                {
                       
                  //  System.out.println("Inside the Version Update Event");  
                    pageContext.writeDiagnostics(this, "Inside ROWREF Logic ----------------------->",4);
                    System.out.println("Inside ROWREF Logic ----------------------->");
                    String rowReference = pageContext.getParameter(OAWebBeanConstants.EVENT_SOURCE_ROW_REFERENCE);    //Obtaining the Row Reference of the current Row
                     obtainRowDtl(rowReference,am, pageContext);
               
      
               }
  
                       
   
  }
    // String rowReference = pageContext.getParameter(OAWebBeanConstants.EVENT_SOURCE_ROW_REFERENCE);    //Obtaining the Row Reference of the current Row
       public void obtainRow(String rowRef, OAApplicationModule am, OAPageContext pageContext)

       {
       OAViewObject dtlVO = (OAViewObject)am.findViewObject("shipDtlVO1");
           OAViewObject soVO = (OAViewObject)am.findViewObject("shipSoEO1");
           OAViewObject staVO = (OAViewObject)am.findViewObject("StatusHideVO1"); 
           soVO.setWhereClauseParams(null);
           soVO.reset();
       OARow row = (OARow)am.findRowByRef(rowRef); //Getting the rowRefence and getting as a row

       if (row!=null)
       {
       System.out.println(row.getAttribute("ShipmtHdrId"));
//           pageContext.writeDiagnostics(this, "Getting the Selected HeaderID ----------------------->"+row.getAttribute("ShipmtHdrId"),4);
           pageContext.writeDiagnostics(this, "Getting the Selected HeaderID ----------------------->"+row.getAttribute("ShipmtHdrId"), 4);
           pageContext.writeDiagnostics(this, "Getting the SEL Value ----------------------->"+row.getAttribute("SEL")+" row.getAttribute(\"SysemStatus\").toString() --------------> "+row.getAttribute("SysemStatus").toString(), 4);
          String sysStat =  row.getAttribute("SysemStatus").toString();
          if(sysStat.equals("Cancelled"))
          {
              staVO.getCurrentRow().setAttribute("CancStatusHideButtons",  Boolean.TRUE); 
              staVO.getCurrentRow().setAttribute("CancButtonSwticher",  Boolean.FALSE); 
              
              pageContext.writeDiagnostics(this, "Inside and Changing the Value to False CancStatusHideButtons----------------------->"+staVO.getCurrentRow().getAttribute("CancStatusHideButtons"), 4);
          }
          else
          {
              staVO.getCurrentRow().setAttribute("CancStatusHideButtons",  Boolean.FALSE); 
              staVO.getCurrentRow().setAttribute("CancButtonSwticher",  Boolean.TRUE); 
              pageContext.writeDiagnostics(this, "Inside and Changing the Value to True CancStatusHideButtons----------------------->"+staVO.getCurrentRow().getAttribute("CancStatusHideButtons"), 4);
          }
          
           if(dtlVO!=null)
           {
           dtlVO.setWhereClause("SHIPMT_HDR_ID = "+(row.getAttribute("ShipmtHdrId")).toString()); 
           dtlVO.executeQuery();
           dtlVO.first();
//               soVO.setWhereClause("SHIPMT_HDR_ID = "+(row.getAttribute("ShipmtHdrId")).toString());
//               soVO.executeQuery();
//               soVO.first();
           System.out.println("Getting the dtlVO Query ----------------------->"+dtlVO.getQuery());
           }
           else
           {
               System.out.println("Value of dtlVO is null");
           }
//       Number CustNum = (Number)row.getAttribute(�CustomerId�); //Obtaining Value from a Attribute and storing in a variable
//       // int CustNum = Integer.parseInt(custNum);
//       System.out.println(�VO getAttribute �+CustNum);
//       OAViewObject addrvo = (OAViewObject)getxxCustAddrVO();
//       addrvo.setWhereClause(�Customer_id = :1�);
//       addrvo.setWhereClauseParams(null);;
//       addrvo.setWhereClauseParam(0,CustNum);
//       addrvo.executeQuery();
       }
       }
       
 public void obtainRowDtl(String rowRef, OAApplicationModule am, OAPageContext pageContext)

 {
     pageContext.writeDiagnostics(this, "Inside obtainRowDtl ---------------------------------------> ", 4);
 OAViewObject dtlVO = (OAViewObject)am.findViewObject("shipDtlVO1");
     OAViewObject soVO = (OAViewObject)am.findViewObject("shipSoEO1");
 OARow row = (OARow)am.findRowByRef(rowRef); //Getting the rowRefence and getting as a row

 if (row!=null)
 {
    try{
     pageContext.writeDiagnostics(this, "Inside  obtainRowDtl ; if (row!=null) the SEL Value ----------------------->"+row.getAttribute("SEL")+" Getting the ShipmtDtlId  Value --------------> "+(row.getAttribute("ShipmtDtlId")).toString(), 4);
     pageContext.writeDiagnostics(this, "Getting the SOVO Query ----------------------->"+soVO.getQuery(), 4);
     if(soVO!=null)
     {
     soVO.setWhereClause("SHIPMT_DTL_ID = "+(row.getAttribute("ShipmtDtlId")).toString()); 
     soVO.executeQuery();
//     soVO.first();
////         soVO.setWhereClause("SHIPMT_HDR_ID = "+(row.getAttribute("ShipmtHdrId")).toString());
////         soVO.executeQuery();
////         soVO.first();
     System.out.println("Getting the soVO Query ----------------------->"+soVO.getQuery());
     }
    
     else
     {
         System.out.println("Value of dtlVO is null");
     }
     }ch(OAException e)
     {
         pageContext.writeDiagnostics(this, "Inside Exception ----------------------->"+soVO.getQuery(), 4);
     }
 //       Number CustNum = (Number)row.getAttribute(�CustomerId�); //Obtaining Value from a Attribute and storing in a variable
 //       // int CustNum = Integer.parseInt(custNum);
 //       System.out.println(�VO getAttribute �+CustNum);
 //       OAViewObject addrvo = (OAViewObject)getxxCustAddrVO();
 //       addrvo.setWhereClause(�Customer_id = :1�);
 //       addrvo.setWhereClauseParams(null);;
 //       addrvo.setWhereClauseParam(0,CustNum);
 //       addrvo.executeQuery();
 }
 }
}
