/*===========================================================================+
 |   Copyright (c) 2001, 2005 Oracle Corporation, Redwood Shores, CA, USA    |
 |                         All rights reserved.                              |
 +===========================================================================+
 |  HISTORY                                                                  |
 +===========================================================================*/
package xxcus.oracle.apps.po.shipment.webui;

import com.sun.java.util.collections.HashMap;

import java.io.Serializable;

import oracle.apps.fnd.common.VersionInfo;
import oracle.apps.fnd.framework.OAApplicationModule;
import oracle.apps.fnd.framework.OAException;
import oracle.apps.fnd.framework.OAViewObject;
import oracle.apps.fnd.framework.server.OADBTransaction;
import oracle.apps.fnd.framework.webui.OAControllerImpl;
import oracle.apps.fnd.framework.webui.OAPageContext;
import oracle.apps.fnd.framework.webui.OAWebBeanConstants;
import oracle.apps.fnd.framework.webui.beans.OAWebBean;

/**
 * Controller for ...
 */
public class AddShipmtCO extends OAControllerImpl
{
  public static final String RCS_ID="$Header$";
  public static final boolean RCS_ID_RECORDED =
        VersionInfo.recordClassVersion(RCS_ID, "%packagename%");

  /**
   * Layout and page setup logic for a region.
   * @param pageContext the current OA page context
   * @param webBean the web bean corresponding to the region
   */
  public void processRequest(OAPageContext pageContext, OAWebBean webBean)
  {
    super.processRequest(pageContext, webBean);
      OAApplicationModule am=null;
      am=(OAApplicationModule)pageContext.getApplicationModule(webBean);
      Serializable[] sn1={"shipmtHdrVO1"};
      am.invokeMethod("createRecord", sn1); 
  }

  /**
   * Procedure to handle form submissions for form elements in
   * a region.
   * @param pageContext the current OA page context
   * @param webBean the web bean corresponding to the region
   */
  public void processFormRequest(OAPageContext pageContext, OAWebBean webBean)
  {
    super.processFormRequest(pageContext, webBean);
      OAApplicationModule am=null;
      am=(OAApplicationModule)pageContext.getApplicationModule(webBean);
      OAViewObject hvo= null;
      hvo=(OAViewObject)am.findViewObject("shipmtHdrVO1");
      OAViewObject hvo2= null;
      hvo2=(OAViewObject)am.findViewObject("shipmtHdrVO2");
      OAViewObject hvoMax= null;
      hvoMax=(OAViewObject)am.findViewObject("shipmtHdrMax1");
        /**=======================================================================
           *            SUBMIT ON ADDSHIPMENT 
           * ====================================================================**/ 
          if(pageContext.getParameter(EVENT_PARAM).equals("SAVE"))
            {
                pageContext.writeDiagnostics(this, "Inside SAVE Logic ----------------------->",4);
                hvoMax.executeQuery();
                hvoMax.first();
                pageContext.writeDiagnostics(this, "Inside AddShipmtCO SAVE Logic -------Get Row count ---------------->"+hvoMax.getRowCount(),4);
                if(hvoMax.getRowCount()>0){
                pageContext.writeDiagnostics(this, "Inside SAVE Logic --------------hvoMax.getCurrentRow().getAttribute(\"ShipmentNumber\")--------->"+hvoMax.getCurrentRow().getAttribute("ShipmentNumber"),4);
                hvo.getCurrentRow().setAttribute("ShipmentNumber", hvoMax.getCurrentRow().getAttribute("ShipmentNumber"));
                pageContext.writeDiagnostics(this, "After Setting the Row and getting Supplier Invoice----------------------->"+hvo.getCurrentRow().getAttribute("SupplierInvoice"),4);                
                }
                hvo2.setWhereClause(" SUPPLIER_INVOICE = nvl('"+(hvo.getCurrentRow().getAttribute("SupplierInvoice")).toString()+"', 0) and SYSEM_STATUS <> 'Cancelled'");
                hvo2.executeQuery();
                pageContext.writeDiagnostics(this, "After Setting the Row ---hvo2.getRowCount()-------------------->"+hvo2.getRowCount(),4);
                if(hvo2.getRowCount()>0)
                {
                    OAException confirmMessage = new OAException("The Shipment Header "+hvo.getCurrentRow().getAttribute("SupplierInvoice")+" already Exists", OAException.ERROR);     
                    System.out.println("Inside SAVE Logic Before throwing Exception ----------------------->");
                    pageContext.putDialogMessage(confirmMessage);
                
                }
                else
                {
                am.invokeMethod("apply");                    
              
                HashMap hm=new HashMap();  //Tried to send parameter through SetFowardURL
                 hm.put("SHIPMTHDRID", hvo.getCurrentRow().getAttribute("ShipmtHdrId"));
                 hm.put("PGMODE", "ADDSHIPMT");
                OADBTransaction txn = am.getOADBTransaction();
                txn.rollback();
                pageContext.setForwardURL("OA.jsp?page=/xxcus/oracle/apps/po/shipment/webui/ShipmtPG",
                null,
                //OAWebBeanConstants.KEEP_NO_DISPLAY_MENU_CONTEXT,
                 OAWebBeanConstants.KEEP_MENU_CONTEXT,
                null,
                hm, //HashMap
                
                false,
                OAWebBeanConstants.ADD_BREAD_CRUMB_YES,
                OAWebBeanConstants.IGNORE_MESSAGES); 
                }
               
            }
            
          if (pageContext.getParameter(EVENT_PARAM).equals("CANCEL"))
          {
              
              OADBTransaction txn = am.getOADBTransaction();
              txn.rollback();
              pageContext.setForwardURL("OA.jsp?page=/xxcus/oracle/apps/po/shipment/webui/ShipmtPG",
              null,
              //OAWebBeanConstants.KEEP_NO_DISPLAY_MENU_CONTEXT,
               OAWebBeanConstants.KEEP_MENU_CONTEXT,
              null,
              null, //HashMap
              
              false,
              OAWebBeanConstants.ADD_BREAD_CRUMB_YES,
              OAWebBeanConstants.IGNORE_MESSAGES); 
          }
  }

}
