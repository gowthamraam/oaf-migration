/*===========================================================================+
 |   Copyright (c) 2001, 2005 Oracle Corporation, Redwood Shores, CA, USA    |
 |                         All rights reserved.                              |
 +===========================================================================+
 |  HISTORY                                                                  |
 +===========================================================================*/
package xxcus.oracle.apps.po.shipment.webui;

import com.sun.java.util.collections.ArrayList;
import com.sun.java.util.collections.HashMap;
import com.sun.java.util.collections.List;

import java.io.Serializable;

import java.sql.CallableStatement;

import java.util.Enumeration;

import oracle.apps.fnd.common.VersionInfo;
import oracle.apps.fnd.framework.OAApplicationModule;
import oracle.apps.fnd.framework.OAException;
import oracle.apps.fnd.framework.OARow;
import oracle.apps.fnd.framework.OAViewObject;
import oracle.apps.fnd.framework.server.OADBTransaction;
import oracle.apps.fnd.framework.webui.OAControllerImpl;
import oracle.apps.fnd.framework.webui.OAPageContext;
import oracle.apps.fnd.framework.webui.OAWebBeanConstants;
import oracle.apps.fnd.framework.webui.beans.OAWebBean;

import oracle.jbo.Row;
import oracle.jbo.domain.Number;

import oracle.jdbc.OracleConnection;
import oracle.jdbc.OraclePreparedStatement;

/**
 * Controller for ...
 */
public class AddDtlsCO extends OAControllerImpl
{
  public static final String RCS_ID="$Header$";
  public static final boolean RCS_ID_RECORDED =
        VersionInfo.recordClassVersion(RCS_ID, "%packagename%");

  /**
   * Layout and page setup logic for a region.
   * @param pageContext the current OA page context
   * @param webBean the web bean corresponding to the region
   */
  public void processRequest(OAPageContext pageContext, OAWebBean webBean)
  {
    super.processRequest(pageContext, webBean);
      pageContext.writeDiagnostics(this, "Inside ShipmtCO We have the Source parameter ----------------------->"+SOURCE_PARAM,4);
        pageContext.writeDiagnostics(this, "Inside ShipmtCO We have the EVENT parameter ----------------------->"+EVENT_PARAM,4);
        System.out.println("We have the Source parameter ----------------------->"+SOURCE_PARAM);
           System.out.println("We have the EVENT parameter ----------------------->"+EVENT_PARAM);
        System.out.println("Getting the SHIPMTHDRID ----------------------->"+pageContext.getParameter("SHIPMTHDRID"));
      pageContext.writeDiagnostics(this, "Getting paramters in  AddDtlsCO ----------------pageContext.getParameter(\"SHIPMTHDRID\")------->"+pageContext.getParameter("SHIPMTHDRID")+" -- pageContext.getParameter(\"POVENDORID\")----->"+pageContext.getParameter("POVENDORID"),4);
  }

  /**
   * Procedure to handle form submissions for form elements in
   * a region.
   * @param pageContext the current OA page context
   * @param webBean the web bean corresponding to the region
   */
  public void processFormRequest(OAPageContext pageContext, OAWebBean webBean)
  {
    super.processFormRequest(pageContext, webBean);
      OAApplicationModule am=null;
      am=(OAApplicationModule)pageContext.getApplicationModule(webBean);
      OAViewObject hvo= null;
      hvo=(OAViewObject)am.findViewObject("shipmtHdrVO1");    
      OAViewObject dtlVO= null;
      dtlVO=(OAViewObject)am.findViewObject("shipDtlVO1");
        OAViewObject dtlreadVO= null;
        dtlreadVO=(OAViewObject)am.findViewObject("shipDtlNEOVO1");
            OAViewObject soVO= null;
            soVO=(OAViewObject)am.findViewObject("shipSoEO1");
        
    
      /**=======================================================================
         *           Search when Find Button Clicked
         * ====================================================================**/
   
      
              if (pageContext.getParameter(EVENT_PARAM).equals("SEARCH"))
              {
                pageContext.writeDiagnostics(this, "Inside Search Logic ----------------------->",4);
                System.out.println("Inside Search Logic ----------------------->");                   
                Enumeration x=null;
                 x=pageContext.getParameterNames();
                          if(x!=null){
                
                          System.out.println("Elements in Enum"+x);
                          while(x.hasMoreElements())
                          {
                          String aParamName = (String)x.nextElement();
                          System.out.println("Obatining the Child Names     : "+aParamName);
                          }
                            System.out.println("VO Gets Queried");
                
                      }
                 
                 
                System.out.println("pageContext.getParameter(\"PoNumber\")"+pageContext.getParameter("PoNumber"));
                System.out.println("pageContext.getParameter(\"PoStatus\")"+pageContext.getParameter("PoStatus"));
                System.out.println("pageContext.getParameter(\"ItemCode\")"+pageContext.getParameter("ItemCode"));
                System.out.println("pageContext.getParameter(\"ItemDescription\")"+pageContext.getParameter("ItemDescription"));
                System.out.println("pageContext.getParameter(\"BuyerName\")"+pageContext.getParameter("BuyerName"));
                
                pageContext.writeDiagnostics(this , "pageContext.getParameter(\"PoNumber\")"+pageContext.getParameter("PoNumber"), 4);
                pageContext.writeDiagnostics(this , "pageContext.getParameter(\"PoStatus\")"+pageContext.getParameter("PoStatus"), 4);
                pageContext.writeDiagnostics(this , "pageContext.getParameter(\"ItemCode\")"+pageContext.getParameter("ItemCode"), 4);
                pageContext.writeDiagnostics(this , "pageContext.getParameter(\"ItemDescription\")"+pageContext.getParameter("ItemDescription"), 4);
                pageContext.writeDiagnostics(this , "pageContext.getParameter(\"BuyerName\")"+pageContext.getParameter("BuyerName"), 4);
 
                
                List whClauseParams = new ArrayList();
                if (pageContext.getParameter("PoNumber") != null && pageContext.getParameter("PoNumber") != "") { 
                    try
                      {
                     // vo.setWhereClauseParam(0, pageContext.getParameter("DocumentName"));
                         whClauseParams.add(" and QRSLT.po_number like '%"+pageContext.getParameter("PoNumber")+ "%'");
                      }
                      catch(Exception e){
                      System.out.println(e);
                      }
                }
                
                
                if (pageContext.getParameter("PoStatus") != null && pageContext.getParameter("PoStatus")!="") { 
                    try
                      {
                     // vo.setWhereClauseParam(0, pageContext.getParameter("DocumentName"));
                         whClauseParams.add(" and QRSLT.po_status like '%"+pageContext.getParameter("PoStatus")+ "%'");
                      }
                      catch(Exception e){
                      System.out.println(e);
                      }
                }
                if (pageContext.getParameter("ItemCode")!= null && pageContext.getParameter("ItemCode")!="") { 
                    try
                      {
                     // vo.setWhereClauseParam(0, pageContext.getParameter("DocumentName"));
                         whClauseParams.add(" and QRSLT.item_code like '%"+pageContext.getParameter("ItemCode")+ "%'");
                      }
                      catch(Exception e){
                      System.out.println(e);
                      }
                }
                if (pageContext.getParameter("ItemDescription") != null &&pageContext.getParameter("ItemDescription")!="") { 
                    try
                      {
                     // vo.setWhereClauseParam(0, pageContext.getParameter("DocumentName"));
                         whClauseParams.add(" and QRSLT.item_description like '%"+pageContext.getParameter("ItemDescription")+ "%'");
                      }
                      catch(Exception e){
                      System.out.println(e);
                      }
                }
                             
                if (pageContext.getParameter("BuyerName") != null &&pageContext.getParameter("BuyerName")!="") { 
                    try
                      {
                     // vo.setWhereClauseParam(0, pageContext.getParameter("DocumentName"));
                         whClauseParams.add(" and QRSLT.buyer_name like '%"+pageContext.getParameter("BuyerName")+ "%'");
                      }
                      catch(Exception e){
                      System.out.println(e);
                      }
                }
                
                  System.out.println("To check VO is not null"+hvo);
                 if(hvo!=null)
                 {
               
                      String sqlparam=" ";
                      if(whClauseParams.isEmpty())
                     {
                                  System.out.println("No Params for Search");
                                  dtlreadVO.setWhereClause("  vendor_id = nvl("+pageContext.getParameter("POVENDORID")+", vendor_id)");      
                                  dtlreadVO.executeQuery();
                                  pageContext.writeDiagnostics(this, "No Params After Execution  ----------------------->"+dtlreadVO.getQuery(),4);
//                                  dtlreadVO.executeQuery();
//                          testVO.executeQuery();
//                          hvo.executeQuery();
//                          hvo.first();
                          System.out.println("Values are testVO.getRowCount() "+hvo.getRowCount());
                          System.out.println("Values are testVO.getFetchedRowCount() "+hvo.getFetchedRowCount());
                          System.out.println("Values are "+hvo.getQuery());

                          System.out.println("Values are dtlreadVO.getRowCount() "+dtlreadVO.getRowCount());
                          System.out.println("Values are dtlreadVO.getFetchedRowCount() "+dtlreadVO.getFetchedRowCount());
                     
//                                  System.out.println("Values are "+dtlreadVO.getQuery());
                      }
                      else
                      {
                                    for (int i = 0; i < whClauseParams.size(); i++) 
                                    {
                                          String k=whClauseParams.get(i)+"";
                                          System.out.println(whClauseParams.get(i));
                                          sqlparam=sqlparam.concat(k);
                                          System.out.println("The param is "+sqlparam);      
                                          pageContext.writeDiagnostics(this, "The for loop param is "+sqlparam, 4);
                                    }
                 
                                      if((sqlparam!=null)&&(sqlparam!=""))
                                      {
                                          sqlparam=sqlparam.substring(5);
                                          sqlparam.concat(" and vendor_id = nvl("+pageContext.getParameter("POVENDORID")+", vendor_id)");
                                          System.out.println("Modified param"+sqlparam);
                                          pageContext.writeDiagnostics(this, "The Modified param is "+sqlparam, 4);
                                          dtlreadVO.setWhereClause(sqlparam);   
                                          pageContext.writeDiagnostics(this, "After Param - Before Execution  Checking----------------------->"+dtlreadVO.getQuery(),4);
//                                          dtlreadVO.setWhereClause(" and vendor_id = nvl("+pageContext.getParameter("POVENDORID")+", vendor_id)");      
                                          dtlreadVO.executeQuery();
                                          dtlreadVO.first();
                                          System.out.println("Values are get Row Count "+dtlreadVO.getRowCount());
                                          System.out.println("Values are "+dtlreadVO.getQuery());
                                          pageContext.writeDiagnostics(this, "After Param - After Execution  ----------------------->"+dtlreadVO.getQuery(),4);
                                          
                                      }
                           
                        }
                  }
            }
      if (pageContext.getParameter(EVENT_PARAM).equals("CANCEL"))
      {
          OADBTransaction txn = am.getOADBTransaction();
          txn.rollback();
          pageContext.setForwardURL("OA.jsp?page=/xxcus/oracle/apps/po/shipment/webui/ShipmtPG",
          null,
          //OAWebBeanConstants.KEEP_NO_DISPLAY_MENU_CONTEXT,
           OAWebBeanConstants.KEEP_MENU_CONTEXT,
          null,
          null, //HashMap
          
          false,
          OAWebBeanConstants.ADD_BREAD_CRUMB_YES,
          OAWebBeanConstants.IGNORE_MESSAGES); 
      }
      
      
   
                 
               
           

      
      if(pageContext.getParameter(EVENT_PARAM).equals("ADDTOSHIPMENT"))
        {
        
//            if (pageContext.getParameter("SelButton") != null)
//                  {
                              System.out.println("Proceed ADDTOSHIPMENT                 ------------->");
            String validateAllo = validateAllocation(am, pageContext);
             
                              
            if( validateAllo.equals("Y")) 
                   {     
//                      OAViewObject vo = (OAViewObject)am.findViewObject("XxFileBlobsVO1");
                     Row row[] = dtlreadVO.getAllRowsInRange();
                    System.out.println("Proceed ADDTOSHIPMENT       --------"+row.length);
                       
                      for (int i=0;i<row.length;i++)
                        {
                        OARow rowi = (OARow)row[i];
                          if (rowi.getAttribute("SelectFlag")!= null && rowi.getAttribute("SelectFlag").equals("Y"))  
//                          if (rowi.getSelectFlag()!= null && rowi.getSelToDel().equals("Y"))
                          {
                              pageContext.writeDiagnostics(this , "rowi.getAttribute(\"SalesOrderNumber\")"+rowi.getAttribute("SalesOrderNumber")+" rowi.getAttribute(\"AllocationQty\") "+rowi.getAttribute("AllocationQty"), 4);
                          if((rowi.getAttribute("SalesOrderNumber")!= null)||(rowi.getAttribute("AllocationQty")!= null))
                          
//                              &&Integer.parseInt(rowi.getAttribute("AllocationQty").toString()) >= 0
                              {
//                              shipDtlVO3
                            System.out.println("Getting Rows       ------PoHeaderId--"+rowi.getAttribute("PoHeaderId")+"---PoLineId--"+rowi.getAttribute("PoLineId")+"---SalesOrderNumber--"+rowi.getAttribute("SalesOrderNumber"));
                                Serializable[] sn1={"shipDtlVO1"};
                                am.invokeMethod("createRecordDtl", sn1); 
                              Serializable[] sn2={"shipSoEO1"};
                              am.invokeMethod("createRecordSo", sn2); 
                              dtlVO.getCurrentRow().setAttribute("ShipmtHdrId", pageContext.getParameter("SHIPMTHDRID"));
                              dtlVO.getCurrentRow().setAttribute("PoHeaderId", rowi.getAttribute("PoHeaderId"));
                              dtlVO.getCurrentRow().setAttribute("PoLineId", rowi.getAttribute("PoLineId"));
                              dtlVO.getCurrentRow().setAttribute("SalesOrderNumber", rowi.getAttribute("SalesOrderNumber"));
                              dtlVO.getCurrentRow().setAttribute("AllocationQty", rowi.getAttribute("AllocationQty"));
                              
                              soVO.getCurrentRow().setAttribute("ShipmtDtlId", dtlVO.getCurrentRow().getAttribute("ShipmtDtlId"));
                              soVO.getCurrentRow().setAttribute("ShipmtHdrId", pageContext.getParameter("SHIPMTHDRID"));
//                              soVO.getCurrentRow().setAttribute("PoHeaderId", rowi.getAttribute("PoHeaderId"));
//                              soVO.getCurrentRow().setAttribute("PoLineId", rowi.getAttribute("PoLineId"));
                              soVO.getCurrentRow().setAttribute("SalesOrderNumber", rowi.getAttribute("SalesOrderNumber"));
                              soVO.getCurrentRow().setAttribute("AllocationQty", rowi.getAttribute("AllocationQty"));
                                  am.invokeMethod("apply");
                                  OAException confirmMessage = new OAException("The Transactions has been saved Successfully", OAException.CONFIRMATION);
                                  System.out.println("Inside SAVE Logic Before throwing Exception ----------------------->");
                                  pageContext.putDialogMessage(confirmMessage);
                              
                              }
                      
                      
                              
//                              OADBTransaction txn=(OADBTransaction)am.getOADBTransaction();
                              
//                              Number seqNoSno1=txn.getSequenceValue("XX_SHIPMT_DTL_ID_S");
//                              rowi.setAttribute("ShipmtDtlId", seqNoSno1);
//                              rowi.setAttribute("ShipmtHdrId", pageContext.getParameter("SHIPMTHDRID"));
    //                            oracle.jbo.domain.Number vFileId = (oracle.jbo.domain.Number)rowi.getFileId();
//                            rowi.remove();   // Here you can call updates if reqd
//                            System.out.println("Selected To Delete File Id "+vFileId);
                          }
                        }
//                        hvo.setWhereClause("SHIPMT_HDR_ID = "+pageContext.getParameter("SHIPMTHDRID"));                            
//                        hvo.executeQuery();
//                        hvo.first();
//                        pageContext.writeDiagnostics(this, "hvo.getFetchedRowCount() :"+hvo.getFetchedRowCount()+" hvo.getRowCount() :"+hvo.getRowCount(),4);
//                        if(hvo.getFetchedRowCount()>0)
//                        {
//                                 pageContext.writeDiagnostics(this, "Inside hvo Setting Confirmation Status",4);
//                                hvo.getCurrentRow().setAttribute("SysemStatus", "Confirmed");
//                        }
                 
            
            HashMap hm=new HashMap();  //Tried to send parameter through SetFowardURL
             hm.put("SHIPMTHDRID", pageContext.getParameter("SHIPMTHDRID"));
             hm.put("PGMODE", "ADDDTLS");
             
            OADBTransaction txn = am.getOADBTransaction();
            txn.rollback();
             
            pageContext.setForwardURL("OA.jsp?page=/xxcus/oracle/apps/po/shipment/webui/ShipmtPG",
            null,
            //OAWebBeanConstants.KEEP_NO_DISPLAY_MENU_CONTEXT,
             OAWebBeanConstants.KEEP_MENU_CONTEXT,
            null,
            hm, //HashMap
            
            false,
            OAWebBeanConstants.ADD_BREAD_CRUMB_YES,
            OAWebBeanConstants.IGNORE_MESSAGES); 
            
        
//        }
        }
        else
        {
            OAException confirmMessage = new OAException(validateAllo , OAException.ERROR);     
            System.out.println("Inside Validation Logic Before throwing Exception ----------------------->");
            pageContext.putDialogMessage(confirmMessage);
        }
        }
        }
        public String validateAllocation(  OAApplicationModule am,OAPageContext pageContext)
        {
            OAViewObject dtlreadVO= null;
            String retVal = null;
            dtlreadVO=(OAViewObject)am.findViewObject("shipDtlNEOVO1");
            Row row[] = dtlreadVO.getAllRowsInRange();
            System.out.println("Proceed ADDTOSHIPMENT       --------"+row.length);
              int counter=0;
             for (int i=0;i<row.length;i++)
               {
               OARow rowi = (OARow)row[i];
                 if (rowi.getAttribute("SelectFlag")!= null && rowi.getAttribute("SelectFlag").equals("Y"))  
            //                          if (rowi.getSelectFlag()!= null && rowi.getSelToDel().equals("Y"))
                 {
                     pageContext.writeDiagnostics(this , "rowi.getAttribute(\"SalesOrderNumber\")"+rowi.getAttribute("SalesOrderNumber")+" rowi.getAttribute(\"AllocationQty\") "+rowi.getAttribute("AllocationQty"), 4);
                     if (rowi.getAttribute("SalesOrderNumber")!= null && rowi.getAttribute("AllocationQty")!=null )  
                                         
                     {
                         pageContext.writeDiagnostics(this , " Sales Order Number and Allocation Quantity is not null -------> ", 4);
                     //   Integer.parseInt(rowi.getAttribute("PendingQty"))rowi.getAttribute("PendingQty")
                      int AvailableQty = Integer.parseInt((rowi.getAttribute("AvailableQty")).toString());
                         int PendingQty = Integer.parseInt((rowi.getAttribute("PendingQty")).toString());
                         int AllocationQty = Integer.parseInt((rowi.getAttribute("AllocationQty")).toString());
                          if(AllocationQty <= 0)
                          {
                              counter = counter+1;
                              pageContext.writeDiagnostics(this, "Inside Allocation Quantity is 0 and throwing Error ",4);
                              retVal = "Allocation Quantity has to be greater than 0";
                              break;
                          }
                         pageContext.writeDiagnostics(this, "Getting the values of AvailableQty "+AvailableQty+" AllocationQty "+AllocationQty+" PendingQty "+PendingQty,4);
                         if(AllocationQty <= AvailableQty && AllocationQty <= PendingQty)
                         {
                         //retVal = "Y";
                          pageContext.writeDiagnostics(this, "Condition is Valid ",4);
                         
                         }
                         else
                         {
                         counter = counter+1;
                         retVal = "Enter the Value Less than the Pending Quantity & Available Quantity";
                         pageContext.writeDiagnostics(this, "Count is getting Increased ",4);
                         }
                     }
                     else
                     {
                         counter = counter+1;
                         retVal = "Sales Order Number and Allocation Quantity has to be Entered";
                     }
                     
                     
                
                 }
               }
              pageContext.writeDiagnostics(this, "Value of Counter is  "+counter,4);
               if(counter>0)
               { 
                return retVal;
               }
               else
               return "Y";
               
             
      
        }

}
