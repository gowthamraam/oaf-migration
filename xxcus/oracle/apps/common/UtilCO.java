package xxcus.oracle.apps.common;

import java.sql.Timestamp;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import oracle.apps.fnd.framework.OAApplicationModule;
import oracle.apps.fnd.framework.OAException;
import oracle.apps.fnd.framework.server.OADBTransactionImpl;
import oracle.apps.fnd.framework.server.OAViewObjectImpl;
import oracle.apps.fnd.framework.webui.OANavigation;
import oracle.apps.fnd.framework.webui.OAPageContext;
import oracle.apps.fnd.framework.webui.beans.OAWebBean;

import oracle.jbo.Row;
import oracle.jbo.domain.Date;

public class UtilCO {


        /**
         * Method to execute View Object
         * @param vo View Object which will be executed
         */
        public void executeVO(OAViewObjectImpl vo)
      {
        if (!vo.isPreparedForExecution())
        {
          vo.executeQuery();
          System.out.println(vo.getName()+" VO Executed.");
        }
      }

        /**
         * Method to check if an object is null or not
         * @param obj -- object which has to be checked
         * @return  -- boolean true if not null, false if null
         */
        public boolean notNull(Object obj)
      {
        if (obj != null && !"".equals(obj))
        {
          return true;
        } else
        {
          return false;
        }
      }

        /** This method copies an attribute from one row to another row.
         * @param fromRow           -- Row from which we need to copy the attribute
         * @param fromAttribute     -- Attribute which has to be copied
         * @param toRow             -- Row to which attribute will be copied
         * @param toAttribute       -- Attribute whose value will be updated.
         */
        public void copyAttribute(Row fromRow, String fromAttribute, Row toRow, 
                                String toAttribute)
      {
        if (notNull(fromRow.getAttribute(fromAttribute)))
        {
            toRow.setAttribute(toAttribute, fromRow.getAttribute(fromAttribute));
        }
      }

        /**
         * This method is for converting string to date.
         * @param stringToConvert  -- The appropriate string which is in correct format
         * @param dateFormat       -- The date format of the passed string
         * @return                 -- The date output of the string passed as input
         */
        public Date stringToDate(String stringToConvert, String dateFormat)
      {
        DateFormat df = null;
        df = new SimpleDateFormat(dateFormat);
        Date dateVal = null;
        try
        {
          java.sql.Date sqlDate = 
            new java.sql.Date(df.parse(stringToConvert).getTime());
          System.out.println("sql date : " + sqlDate);
          dateVal = new oracle.jbo.domain.Date(sqlDate);
          System.out.println("Date value is : " + dateVal);
        } ch (ParseException e)
        {
          e.printStackTrace();
          throw new OAException("Error in StrintoDate Method : " + e.toString());
        }
        return dateVal;
      }


        /**
         * This method is for converting string to timestamp.
         * @param stringToConvert  -- The appropriate string which is in correct format
         * @param dateFormat       -- The date format of the passed string.
         * @return                 -- The timestamp format of the string passed as input
         */
      public Date stringToTimeStamp(String stringToConvert, 
                                           String dateFormat)
      {
        DateFormat df = null;
        df = new SimpleDateFormat(dateFormat);
        Date dateVal = null;
        try
        {
          java.sql.Timestamp sqlDate = 
            new Timestamp(df.parse(stringToConvert).getTime());
          System.out.println("sql date : " + sqlDate);
          dateVal = new oracle.jbo.domain.Date(sqlDate);
          System.out.println("Date value is : " + dateVal);
        } ch (ParseException e)
        {
          e.printStackTrace();
          throw new OAException("Error in StrintoTimestamp Method : " + 
                                e.toString());
        }
        return dateVal;
      }

        /**
         * This method is used to write log information if logging is enabled.
         * @param am            -- The calling controller's Application module
         * @param logLevel      -- Log level
         * @param logMessage    -- Log message to write.
         */
        public void writeLog(OAApplicationModule am,int logLevel,String logMessage)
        {
          if(am.getOADBTransaction().isLoggingEnabled(logLevel))
          {
            am.getOADBTransaction().writeDiagnostics(this,logMessage,logLevel);
          }
        }

        /**
         * The method is used to trigger workflow from the page
         * @param pageContext   -- the current OA page context
         * @param webBean       -- the web bean corresponding to the region
         * @param itemType      -- The item type of the workflow to launch
         * @param process       -- The process of the workflow to launch
         * @param itemKey       -- The item key of the workflow to launch
         */
        public void launchWF(OAPageContext pageContext,OAWebBean webBean,String itemType,String process,String itemKey)
        {
        OAApplicationModule am=pageContext.getApplicationModule(webBean);
        OADBTransactionImpl OADBTxn = (OADBTransactionImpl)am.getOADBTransaction();
        OANavigation wf = new OANavigation();
        wf.createProcess(pageContext, itemType, process, itemKey);
        wf.setItemOwner(pageContext,itemType,itemKey,OADBTxn.getUserName());
        wf.startProcess(pageContext, itemType, process, itemKey);        
        }
        

}
