SET PAGESIZE 0 FEEDBACK OFF VERIFY OFF HEADING OFF ECHO OFF LONG 999999 LONGCHUNKSIZE 999999 LINESIZE 500
-- =========================================================================
-- $header$
--
-- name      : xxadt_grievances_sy.sql
--
-- description     : synonym creation scripts for  xxadt_grievance custom module
--
--
-- who                                            remarks                        date
-- --------------                  --------------------------------------- ------------------
-- gowtham raam.j -4iapps                     0.1 - dev version               02-jun-2014
-- =========================================================================
/*     Synonyms     :     Tables  */



create synonym apps.xx_shipment_hdr for xxcus.xx_shipment_hdr;          
create synonym apps.xx_shipment_dtl for xxcus.xx_shipment_dtl;             
create synonym apps.xx_shipment_so for xxcus.xx_shipment_so;
 

/*     Synonyms     :    Sequences*/

create synonym apps.xx_shipmt_hdr_id_s for xxcus.xx_shipmt_hdr_id_s;
create synonym apps.xx_shipmt_dtl_id_s for xxcus.xx_shipmt_dtl_id_s;
create synonym apps.xx_shipmt_so_id_s for xxcus.xx_shipmt_so_id_s;


EXIT;
EOF